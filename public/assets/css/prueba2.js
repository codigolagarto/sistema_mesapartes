let forma = document.querySelector('.form-register_dos');
let progressOptionss = document.querySelectorAll('.progressbar__option2');

forma.addEventListener('click', function(e) {
    let element = e.target;
    let isButtonNext = element.classList.contains('step__button--next2');
    let isButtonBack = element.classList.contains('step__button--back2');
    if (isButtonNext || isButtonBack) {
        let currentStep = document.getElementById('stepp-' + element.dataset.step);
        let jumpStep = document.getElementById('stepp-' + element.dataset.to_step);
        currentStep.addEventListener('animationend', function callback() {
            currentStep.classList.remove('active');
            jumpStep.classList.add('active');
            if (isButtonNext) {
                currentStep.classList.add('to-left');
                progressOptionss[element.dataset.to_step - 1].classList.add('active');
            } else {
                jumpStep.classList.remove('to-left');
                progressOptionss[element.dataset.step - 1].classList.remove('active');
            }
            currentStep.removeEventListener('animationend', callback);
        });
        currentStep.classList.add('inactive');
        jumpStep.classList.remove('inactive');
    }
});