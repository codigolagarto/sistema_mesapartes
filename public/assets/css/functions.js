//alert('hola mundo');
$(document).ready(function(){
    $('#foto').on('change', function(){
   
       var uploadFoto=document.getElementById('foto').value;
       var foto=document.getElementById('foto').files;
       var nav=window.URL || window.webkitURL;
       var contacAlert =document.getElementById('form_alert');
       if(uploadFoto!=''){
            var type = foto[0].type;
            var name = foto[0].type;
            if(type!= 'image/jpeg' &&type!= 'image/jpg' && type!= 'image/png'){
                contacAlert="<p class='errorArchivo'>El archivo no es valido</p>";
                $('#img').remove();
                $('.delPhoto').addClass('notBlock');
                $('#foto').val('');
                
            }else{
                contacAlert.innerHTML='';
                $('#img').remove();
                $('.delPhoto').removeClass('notBlock');
                var obejto_url=nav.createObjectURL(this.files[0]);
                $('.prevPhoto').append("<img id='img' src="+objeto_url+">");
                $('.uping lavel').remove();
            }
       }else{
           alert('no selecciono foto');
           $('#img').remove();
   
       }
    });
   
    $('.delPhoto').click(function(){
           $('#foto').val('');
           $('.delPhoto').addClass('notBlock');
           $('#img').remove();
   
          // if($('#foto_actual') && $('#foto_remove')){
            //   $('#foto_remove').val('img_producto.png');
           //}
   
        });
   // modal para agregar
   $('.add_product').click(function(e){
           e.preventDefault();
           var producto = $(this).attr('product');
           alert(producto);
           $('.modal').fadeIn();
   });
       
        
        //modal de agregar presupuesto
        $('.crear_diagnostico').click(function(e){
            e.preventDefault();
            var presupuesto = $(this).attr('fac');
            var action = 'crearDiagnostico';

            $.ajax({
             url:'ajax.php',
             type: 'POST',
             async: true,
             data:{action:action, presupuesto:presupuesto},
             success:function (response) 
             {    
                 //console.log(response);
                 if(response!='error'){
                     //console.log('distinto de error');
                   //  var info= JSON.parse(response);
                    //console.log(info);
                     
                 }
               }, error:function(error){
                console.log(error);
            }
            });
            $('.modal').fadeIn();
        });
      

        //modal de eliminar presupuesto
        $('.anular_factura').click(function(e){
            e.preventDefault();
            var presupuesto = $(this).attr('fac');
            var action = 'infoPresupuesto';
            console.log(presupuesto);
            $.ajax({
             url:'ajaxP.php',
             type: 'POST',
             async: true,
             data:{action:action, presupuesto:presupuesto},
             success:function (response) 
             {    
                 //console.log(response);
                 if(response!='error'){
                     //console.log('distinto de error');
                     var info= JSON.parse(response);
                    console.log(info);
                     $('.bodyModal_1').html(
                        '<form action="" method="Post" name="form_anular_presupuesto" id="form_anular_presupuesto" onsubmit="event.preventDefault(); anularFactura();">'+
                        '<h1 style="color: white;"><i class="fas fa-exclamation-triangle" style="font-size:45pt; color: white;"></i><br> Anular Presupuesto </h1>'+
                        '<p>Esta seguro de Anular el presupuesto?</p>'+

                       
                        '<p><strong>Nro. ' + info.nro_pre+ '</strong></p>'+
                        '<p><strong>Precio Total. ' + info.totalfactura+ ' Bs.</strong></p>'+
                        '<p><strong>Fecha. ' + info.fecha+ '</strong></p>'+

                        '<input type="hidden" name="action" value="anularFactura">'+
                        '<input type="hidden" name="no_factura" id="no_factura" value="'+info.nro_pre+'" required>'+

                        '<div class="alert alertAddProduct"></div>'+
                      '<button type="submit" class="btn_anular_g"> Guardar</button>'+'<button type="reset" class="btn_anular_2" onclick="coloseModal();"> Cancelar</button>'+
                        '</form>'
                     );

                   
                 }
               }, error:function(error){
                console.log(error);
            }
            });
            $('.modal_1').fadeIn();
        });
      
        //ver presupuesto
        $('.view_factura').click(function(e){
              e.preventDefault();
              var cliente = $(this).attr('cl');
              var presupuesto = $(this).attr('f');
              generarPDF(cliente, presupuesto);
        });
        
        $('#search_proveedor').change(function(e){
            e.preventDefault();
            var sistema= getUrl();
            location.href =sistema+ 'buscar_productos.php?provedor='+$(this).val();
   
        });

        //activa los campos para registrar cliente
     
        $('#btn_new_cliente').slideUp()
        //buscar cliente
        $('#nit_cliente').keyup(function(e){ //el keyup
        e.preventDefault();
        var cl=$(this).val();
        console.log(cl);
       // $('.btn_new_cliente').slideDown();
        var action ='searchCliente';
                // estructura de ajax
                $.ajax({
                url: 'ajaxP.php',
                type: 'POST',
                async:true,
                data:{action:action, cliente:cl},
                success:function(response){
                    console.log(response);
                         if(response==0){   //limpia los campos si esta en cero
                            //bloquear campos
                            $('#idseccion').attr('disabled', 'disabled');
                            $('#idsubsec').attr('disabled', 'disabled');
                            $('#idserie').attr('disabled', 'disabled');
                            $('#idsubserie').attr('disabled', 'disabled');
                            $('#idfondo').attr('disabled', 'disabled');
                            $('#idunidoc').attr('disabled', 'disabled');
                            $('#idcodref').attr('disabled', 'disabled');
                            $('#idfechaini').attr('disabled', 'disabled');
                            $('#idfechafin').attr('disabled', 'disabled');
                            $('#btn_new_cliente').slideUp();
                            
                        }else{
                             // si hay usuario
                            var data =JSON.parse(response);
                            $('#idcliente').val(data.idpersona);
                            $('#nom_cliente').val(data.nombres);
                            $('#idam').val(data.ap_materno);
                            $('#idap').val(data.ap_paterno);
                            $('#idcel').val(data.telefono);
                            $('#iddirec').val(data.direccion);
                            $('#idcorre').val(data.email);
                            $('#idfondo').val('Autoridad de Fiscalización y Control de Cooperativas');
                            
                            
                            $('#btn_new_cliente').slideDown();// ocultar el boton de agregar
                             //bloquear campos
                            $('#nom_cliente').attr('disabled', 'disabled');
                            $('#idam').attr('disabled', 'disabled');
                            $('#idap').attr('disabled', 'disabled');
                            $('#idam').attr('disabled', 'disabled');
                            $('#iddirec').attr('disabled', 'disabled');
                            $('#idcorre').attr('disabled', 'disabled');
                            $('#idcel').attr('disabled', 'disabled');

                            $('#idfondo').attr('disabled', 'disabled');
                            $('#idserie').removeAttr('disabled');
                            $('#idseccion').removeAttr('disabled');
                            $('#idsubsec').removeAttr('disabled');
                            $('#idsubserie').removeAttr('disabled');
                            $('#idunidoc').removeAttr('disabled');
                            $('#idcodref').removeAttr('disabled');
                            $('#idfechaini').removeAttr('disabled');
                            $('#idfechafin').removeAttr('disabled');
                             //oculta el boton de guardar
                            
                           
                        }
                    }, 
                    error: function(error){                  }
        
                });
   
        });
 
        $('#siguiente_btn').slideUp();
        //buscar cliente
        $('#idregistro').keyup(function(e){ //el keyup
            e.preventDefault();
            var cl=$(this).val();
            //console.log(cl);
           // $('.btn_new_cliente').slideDown();
            var action ='searchCoope';
                    // estructura de ajax
                    $.ajax({
                    url: 'ajaxPG.php',
                    type: 'POST',
                    async:true,
                    data:{action:action, cliente:cl},
                    success:function(response){
                        //console.log(response);
                        if(response==''){
                            $('#siguiente_btn').slideUp();
                        }else{
                       
                           var data =JSON.parse(response);
                            $('#iddenominacion').val(data.denominacion);
                            $('#idsector').val(data.sector);
                            $('#idclase').val(data.clase);
                            $('#idsubclase').val(data.subclase);
                            $('#idnr').val('NR');
                            $('#idfecha').val(data.fecha_creacion);
                            //$('#idfondo').val('Autoridad de Fiscalización y Control de Cooperativas');
                            
                            
                            //$('#btn_new_cliente').slideDown();// ocultar el boton de agregar
                             //bloquear campos
                           

                          
                            $('#idnrocaja').removeAttr('disabled');
                            $('#idtomo').removeAttr('disabled');
                            $('#idcodrefe').removeAttr('disabled');
                            $('#idsubserie').removeAttr('disabled');
                            $('#idunidoc').removeAttr('disabled');
                            $('#idcodref').removeAttr('disabled');
                            $('#idfechaini').removeAttr('disabled');
                            $('#idfechafin').removeAttr('disabled');
                             //oculta el boton de guardar
                             $('#siguiente_btn').slideDown();
                        }
                        
                        }, 
                        error: function(error){                  }
            
                    });
    
        });
        //crear cliente
        $('#formulario_t').submit(function(e){
            console.log('entrando...');
                e.preventDefault();
            
        
                     // estructura de ajax
                    $.ajax({
                        url: 'ajaxP.php',
                        type: 'POST',
                        async:true,
                        data:$('#formulario_t').serialize(),//sirve para serializar la 
        
                        success:function(response){
                       // console.log(response);
                    if(response=='exitoso...'){
            
                        location.reload();  
                         // location.reload(); 
                    } else {
                        console.log(response);
                    ///  location.reload(); 
                    }
                            
                        }, 
                        error: function(error){
        
                        }
                        });
        });
    
        $("#NuevoPDF").click(function () {
            $("#NuevoPDF").attr("href", "/Sistema_MesaPartes/" + archi);
        });
    
//registro de documentos 2
$('#formulario-tramite_doc').submit(function(event) {
    event.preventDefault();
    var isChecked = document.getElementById('check').checked;
                if(isChecked){
                    if(ValidarPDFs()){
                    Swal.fire({
                        title: '¿Estás seguro?',
                        text: "Se registrará su trámite",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: 'Enviar'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            var formData = new FormData($(this)[0]);
                            $.ajax({
                                url: 'ajaxP.php',
                                type: 'POST',
                                data: formData,
                                async: false,
                                cache: false,
                                contentType: false,
                                processData: false,
                                success: function(response) {
                                    // Manejar la respuesta del servidor aquí
                                //    console.log(response);
                                if(response!='error'){
                                    location.reload();  
                                }else{
                                    alert('error');
                                }
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                    console.log(textStatus, errorThrown);
                                }
                            });
                                
                        }
                    }) 
                    return false;
                    }else{
                    Swal.fire({ 
                        icon: 'error',
                        //title: 'Solo se permite archivos tipo PDF',
                        html: 'El archivo no es de tipo pdf',
                    })
                    return false;
                    }
                }else{
                    alert('Por favor complete todos los campos requeridos');
                } 
    return false;
});

        //registro cooperativa
        $('#formulario-tramite_doc_dos').submit(function(e){
            console.log('entrando...');
                e.preventDefault();
                var isChecked = document.getElementById('check2').checked;
                if(isChecked){
                    if(ValidarPDF2()){
                    Swal.fire({
                        title: '¿Estás seguro?',
                        text: "Se registrará su trámite",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: 'Enviar'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            var formData = new FormData($(this)[0]);
                            $.ajax({
                                url: 'ajaxP.php',
                                type: 'POST',
                                data: formData,
                                async: false,
                                cache: false,
                                contentType: false,
                                processData: false,
                                success: function(response) {
                                    // Manejar la respuesta del servidor aquí
                                //    console.log(response);
                                if(response!='error'){
                                    location.reload();  
                                }else{
                                    alert('error');
                                }
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                    console.log(textStatus, errorThrown);
                                }
                            });
                                
                        }
                    }) 
                    return false;
                    }else{
                    Swal.fire({ 
                        icon: 'error',
                        //title: 'Solo se permite archivos tipo PDF',
                        html: 'El archivo no es de tipo pdf',
                    })
                    return false;
                    }
                }else{
                    alert('Por favor complete todos los campos requeridos');
                } 
        });


        $('.actualizar_boton').slideUp();
        $('.editar_documento').click(function(e){
            e.preventDefault();//previene a que se recargye la pagina
            $('#idserie').removeAttr('disabled');
            $('#idseccion').removeAttr('disabled');
            $('#idsubsec').removeAttr('disabled');
            $('#idsubserie').removeAttr('disabled');
            $('#idunidoc').removeAttr('disabled');
            $('#idcodref').removeAttr('disabled');
            $('#idfechaini').removeAttr('disabled');
            $('#idfechafin').removeAttr('disabled');
            $('#idmeli').removeAttr('disabled');
            //mostrar boton agregar
            $('.actualizar_boton').slideDown();
            $('.editar_documento').slideUp();
    
        });
    
 //editar 
$('#formulario_edit').submit(function(e){
    e.preventDefault();
     // estructura de ajax
    $.ajax({
        url: 'ajaxP.php',
        type: 'POST',
        async:true,
        data:$('#formulario_edit').serialize(),//sirve para serializar la 

        success:function(response){
       // console.log(response);
            //console.log()
            if(response=='cambiado'){
            
                location.reload();  
                 // location.reload(); 
            } else {
                console.log(response);
            ///  location.reload(); 
            }
        }, 
        error: function(error){

        }
        });
});
    
      //crear cliente
        $('#form_editar_DT').submit(function(e){
                    e.preventDefault();
                     // estructura de ajax
                    $.ajax({
                        url: 'ajaxP.php',
                        type: 'POST',
                        async:true,
                        data:$('#form_editar_DT').serialize(),//sirve para serializar la 
        
                        success:function(response){
                        console.log(response);
                                
                            if(response!=0){
                                //bloquear campos
                                $('.alertAddProduct').html('<p> Actualizado correctamente </p>');
                                $('#div_registro_cliente').slideUp();
                                location.reload(); 
                                 // location.reload(); 
                            } else {
                                $('.alertAddProduct').html('<p style="color:red;"> Ya exite otro registro similar</p>');
                            ///  location.reload(); 
                            }
                            
                        }, 
                        error: function(error){
        
                        }
                        });
        });
});



function generarPDF(cliente, factura){
    var ancho = 1000;
    var alto = 800;
    //calcular la posicion x, y para centrar l ventana

    var x=parseInt((window.screen.width/2) - (ancho/2));
    var y=parseInt((window.screen.height/2) - (alto/2));

    $url ='factura/generaFactura.php? cl='+ cliente + '&f='+ factura;
    window.open($url, "Factura", "left="+x+",top="+y+",height="+alto+",width="+ancho+", scrollbar=si, location=no, resizable=si, menubar=no");
}
   function viewProcesar(){
    if($('#detalle_venta tr').length>0){
        $('#btn_facturar_venta').show();

    }else{
        $('#btn_facturar_venta').hide();
    }
}

function searchForDetalle(id){
    var action ='searchForDetalle';
    var user =id;
    $.ajax({
        url: 'ajaxP.php',
        type: 'POST',
        async:true,
        data: { action: action, user:user},

            success: function(response)
            {
              // console.log(response);
              if(response !=0){
                
                 var info =JSON.parse(response);
          
                  $('#detalle_venta').html (info.detalle);
                  $('#detalle_totales').html(info.totales);
                
                  //activar
                  $('#respuesta').removeAttr('disabled');

              }else{
                 console.log('no function');
              }
               //llamamos a la funcion
               viewProcesar();
            },
            error: function (error) {
                
            }

    });
}
function getUrl(){
    var loc=window.location;
    var pathName= loc.pathname.substring(0, loc.pathname.lastIndexOf('/')+1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname+loc.search+loc.hash).length +pathName.length));

}
// para los modales

function coloseModal(){
    $('.modal_1').fadeOut();
}

// para anular la factura

function anularFactura(){
     var nofactura = $('#no_factura').val();
    // console.log('....1....');
    // console.log(nofactura);
     var action = 'anularFactura';
     $.ajax({
          url: 'ajaxP.php',
          type: "POST",
          async: true,
          data: {action:action, nofactura:nofactura},
          success: function(response){
             console.log(response);
             if(response == 'error'){
                 $('.alertAddProduct').html('<p style="color:red;"> Error al anular el presupuesto. </p>');
                  location.reload(); 
             }else{
                 $('#row_'+nofactura+ '.estado').html('<span class="anulada">Anulada</span>');
                 $('#form_anular_presupuesto .btn_ok').remove();
                 $('#row_'+nofactura+' .div_factura').html('<button type="button" class="btn_anular inactive"><i  class="fas fa-ban"></i></button>');
                 $('.alertAddProduct').html('<p> Presupuesto anulado. </p>');
                 location.reload(); 
             }
          }, 
          error: function(error){

          }

     });
}

function ValidarPDFs(){
    var archivo = document.getElementById("idfile10").value;
   // console.log(archivo);
   
    var extensiones = archivo.substring(archivo.lastIndexOf("."));
  
    if(extensiones != ".pdf" ){
    return false;
    }else{
    return true;
    }

  }

  function ValidarPDF2(){

    var archivo2 = document.getElementById("idfile20").value;

    var extensiones2 = archivo2.substring(archivo2.lastIndexOf("."));

    if( extensiones2 != ".pdf" ){
        return false;
    }else{
        return true;
    }
  }