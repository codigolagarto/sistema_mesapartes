<?php
include("../config/conexion.php");


$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http";
$host = $_SERVER['HTTP_HOST'];
$url_base = "{$protocol}://{$host}/Sistema_MesaPartes/";

if(isset($_SESSION["idusuarios"])){
    header("Location: {$url_base}view/Home/");
    exit;
}

if(isset($_POST["enviar"]) and $_POST["enviar"]=="si"){
    require_once("../models/Usuario.php");
    $usuario = new Usuario();
    $usuario->login();
}
?>


<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ingreso al Sistema</title>
    <link rel="stylesheet" href="../public/assets/css/login.css">
    <link rel="shortcut icon" href="../public/assets/img/logo.png">

</head>

<body>
<img src="../public/assets/img/imagen101.png" alt="Imagen de fondo" class="imagenDeFondo">
    <div class="formulario">
    
        <img class="logo" src="../public/assets/img/img1.jpg"/>
        <h1>Inicio de Sesión</h1>
        <?php
                if (isset($_GET["m"])){
                     switch($_GET["m"]){
                         case "1";
        ?>
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="d-flex align-items-center justify-content-start">
                        <i class="icon ion-ios-checkmark alert-icon tx-32 mg-t-5 mg-xs-t-0"></i>
                        <span style="font-size:14px; align-text:center;">&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                         !! El Usuario y/o la contraseña son incorrectos.. !! </span>
                    </div>
                </div>
        <?php
        break;

        case "2";
        ?>
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="d-flex align-items-center justify-content-start">
                        <i class="icon ion-ios-checkmark alert-icon tx-32 mg-t-5 mg-xs-t-0"></i>
                        <span style="font-size:14px; align-text:center;">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                         !! Ingrese sus credenciales..  !! </span>
                    </div>
                </div>
        <?php
        break;
        }
        }
        ?>
        <form method="post" action="">

            <div class="username">
                <input type="text" name="usuario" required>
                <label><b> Usuario:</b></label>
            </div>
            <div class="username">
                <input type="password" required name="password">
                <label><b>Contraseña:</b></label>
            </div>

            <input type="hidden" name="enviar" value="si">
            <input type="submit" value="Ingresar"><br><br>
            <div class="recordar"><a href="../RecuperarContrasena/">¿Olvidó su contraseña?</a></div>
        </form>
    </div>
</body>

</html>