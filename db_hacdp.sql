-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-03-2024 a las 13:49:36
-- Versión del servidor: 10.4.27-MariaDB
-- Versión de PHP: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_hacdp`
--

DELIMITER $$
--
-- Funciones
--
CREATE DEFINER=`root`@`localhost` FUNCTION `gen_nroexpediente` () RETURNS VARCHAR(6) CHARSET utf8mb4 COLLATE utf8mb4_general_ci  Begin

  declare con int default 0;

  declare cadena varchar(6) default '';

	set con=(select max(iddocumento) from documento);

  if(con is null) then

    set cadena='000001';

    else if((con+1)<10) then

       set cadena=concat('00000',con+1);

      else if((con+1)<100) then

          set cadena=concat('0000',con+1);

          else if((con+1)<1000) then

              set cadena=concat('000',con+1);

              else if((con+1)<10000) then

                 set cadena=concat('00',con+1);

                  else if((con+1)<100000) then

                     set cadena=concat('0',con+1);

                     else if((con+1)<1000000) then

                         set cadena=concat('',con+1);

                      end if;

                  end if;

              end if;

          end if;

      end if;

    end if;

  end if;

Return cadena;



End$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area`
--

CREATE TABLE `area` (
  `idarea` int(11) NOT NULL,
  `cod_area` varchar(15) NOT NULL,
  `area` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `area`
--

INSERT INTO `area` (`idarea`, `cod_area`, `area`) VALUES
(1, '0100', 'ACTIVOS FIJOS'),
(2, '0101', 'RECAUDACIONES'),
(3, '0102', 'JURIDICA'),
(5, '0103', 'TESORERIA'),
(6, '0104', 'UNIDAD DE TECNOLOGIA DE LA INFORMACION (UTI)'),
(7, '0105', 'PLATAFORMA'),
(8, '0106', 'ARCHIVO CENTRAL'),
(11, '0107', 'RECURSOS HUMANOS'),
(12, '0108', 'DIRECCION DE CONTROL Y FISCALIZACION (DCF)'),
(16, '000001', 'ADMIN SISTEMA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `areainstitu`
--

CREATE TABLE `areainstitu` (
  `idareainstitu` int(11) NOT NULL,
  `idinstitucion` int(11) NOT NULL,
  `idarea` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `areainstitu`
--

INSERT INTO `areainstitu` (`idareainstitu`, `idinstitucion`, `idarea`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(11, 1, 11),
(12, 1, 12),
(16, 1, 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `derivacion`
--

CREATE TABLE `derivacion` (
  `idderivacion` int(11) NOT NULL,
  `fechad` datetime NOT NULL,
  `origen` varchar(100) NOT NULL,
  `idareainstitu` int(11) NOT NULL,
  `iddocumento` int(11) NOT NULL,
  `descripcion` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `derivacion`
--

INSERT INTO `derivacion` (`idderivacion`, `fechad`, `origen`, `idareainstitu`, `iddocumento`, `descripcion`) VALUES
(109, '2024-02-27 12:25:10', 'EXTERIOR', 8, 50, ''),
(110, '2024-02-27 14:49:19', '3', 8, 51, ''),
(111, '2024-02-27 15:08:29', 'ARCHIVO', 8, 52, ''),
(112, '2024-02-27 15:52:46', 'ARCHIVO 2', 8, 53, ''),
(113, '2024-02-28 12:15:07', 'ARCHIVO 5', 8, 54, ''),
(114, '2024-02-28 15:41:33', 'SED', 8, 54, ''),
(115, '2024-02-28 15:47:28', 'SDFS', 8, 55, ''),
(116, '2024-02-28 17:34:03', 'RE', 8, 56, ''),
(117, '2024-02-28 18:21:15', 'ASFA', 8, 56, ''),
(118, '2024-02-28 18:24:03', 'MKL', 8, 56, ''),
(119, '2024-02-28 18:35:16', 'FGH', 8, 57, ''),
(120, '2024-03-01 15:42:28', 'ASF', 8, 58, ''),
(121, '2024-03-01 16:15:54', 'ASF', 8, 59, ''),
(122, '2024-03-01 16:22:18', 'JURIDICA', 8, 60, ''),
(123, '2024-03-01 17:35:42', 'ASFAS', 8, 61, ''),
(124, '2024-03-01 17:43:38', 'SDFGSD', 8, 62, ''),
(125, '2024-03-01 18:25:41', 'ASFASF', 8, 63, ''),
(126, '2024-03-04 16:23:52', 'ARCHIVO CENTRAL', 6, 50, 'VERIFIQUE QUE EL DOCUMENTO NO COINCIDE LAS FOJAS'),
(127, '2024-03-04 16:27:02', 'ARCHIVO CENTRAL', 6, 51, 'FOLIOS NO COINCIDEN'),
(128, '2024-03-04 18:42:54', 'ASDAS', 8, 64, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentacion`
--

CREATE TABLE `documentacion` (
  `id_orden` int(11) NOT NULL,
  `codigo_referencia` int(11) NOT NULL,
  `nro_caja` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_final` date NOT NULL,
  `tomo` int(11) NOT NULL,
  `fojas` int(11) NOT NULL,
  `soporte_fisico` int(11) NOT NULL,
  `nombre_unidad` varchar(200) NOT NULL,
  `estante` int(11) NOT NULL,
  `cuerpo` int(11) NOT NULL,
  `valda` int(11) NOT NULL,
  `observaciones` varchar(220) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `documentacion`
--

INSERT INTO `documentacion` (`id_orden`, `codigo_referencia`, `nro_caja`, `titulo`, `fecha_inicio`, `fecha_final`, `tomo`, `fojas`, `soporte_fisico`, `nombre_unidad`, `estante`, `cuerpo`, `valda`, `observaciones`) VALUES
(1, 2, 646, 'GSDG', '2024-03-02', '0000-00-00', 65, 0, 0, '56', 5, 56, 65, 'ZF'),
(2, 67, 67, 'SDFSDG', '2024-02-09', '0000-00-00', 0, 65, 76, 'SDGSDG', 8, 7, 97, 'HDFH'),
(3, 32, 5, 'CONTRATOS ANUALES', '2023-08-11', '2023-08-25', 30, 369, 58, 'UTI', 3, 7, 8, 'COMPLETO'),
(4, 6, 78, 'SFSG', '2024-01-12', '2024-05-10', 6, 8, 9, 'SDGSDG', 8, 7, 8, 'DFGFD'),
(5, 547, 57, 'GSRFF', '2024-01-12', '2024-02-03', 0, 54, 45, 'ASFA', 7, 67, 67, 'GDG'),
(6, 234, 43, 'ASFSAF', '2024-02-01', '2024-02-16', 6, 7, 56, 'ASFAF', 9, 89, 89, 'GDGSD'),
(7, 123, 1, 'ASDASDA', '2024-03-06', '2024-03-08', 1, 1, 2, 'ASDASD', 1, 2, 2, 'ASDASD');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documento`
--

CREATE TABLE `documento` (
  `iddocumento` int(11) NOT NULL,
  `nro_expediente` varchar(10) NOT NULL,
  `nro_doc` varchar(10) NOT NULL,
  `folios` int(11) NOT NULL,
  `asunto` varchar(500) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `archivo` text NOT NULL,
  `idpersona` int(11) NOT NULL,
  `idtipodoc` int(11) NOT NULL,
  `idubi` int(11) NOT NULL DEFAULT 0,
  `fondo` varchar(50) DEFAULT NULL,
  `seccion` varchar(100) NOT NULL,
  `id_sub_seccion` int(11) NOT NULL,
  `serie` varchar(100) NOT NULL,
  `id_sub_serie` int(11) NOT NULL,
  `codigo_referencia` varchar(100) NOT NULL,
  `fecha_inicio` date NOT NULL DEFAULT curdate(),
  `fecha_final` date NOT NULL DEFAULT curdate(),
  `unidades_documentales` int(11) NOT NULL,
  `metros_lineales` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `documento`
--

INSERT INTO `documento` (`iddocumento`, `nro_expediente`, `nro_doc`, `folios`, `asunto`, `estado`, `archivo`, `idpersona`, `idtipodoc`, `idubi`, `fondo`, `seccion`, `id_sub_seccion`, `serie`, `id_sub_serie`, `codigo_referencia`, `fecha_inicio`, `fecha_final`, `unidades_documentales`, `metros_lineales`) VALUES
(50, '000001', '223', 33, 'FASF', 'ACEPTADO', 'files/docs/000001_2024_91062400.pdf', 58, 2, 6, NULL, '', 0, '', 0, '', '2024-02-28', '2024-02-28', 0, 0),
(51, '000051', '23', 23, 'AS', 'PENDIENTE', 'files/docs/000051_2024_91062400.pdf', 58, 3, 6, NULL, '', 0, '', 0, '', '2024-02-28', '2024-02-28', 0, 0),
(52, '000052', '221', 31, 'COMPLETO', 'ARCHIVADO', 'files/docs/000052_2024_91062400.pdf', 58, 2, 8, 'ARCHIVO', '', 0, '', 0, '', '2024-02-28', '2024-02-28', 0, 0),
(53, '000053', '21', 32, 'REGLAMENTO', 'ACEPTADO', 'files/docs/000053_2024_91062400.pdf', 58, 3, 8, 'ARCHIVO 2', 'DIRECCION GENERAL EJECUTIVA', 0, '', 0, '', '2024-02-28', '2024-02-28', 0, 0),
(54, '000054', '12', 32, 'FASFSAF', 'PENDIENTE', 'files/docs/000054_2024_91062400.pdf', 58, 4, 8, 'ARCHIVO 5', 'UTI', 3, '', 0, '', '2024-02-28', '2024-02-28', 0, 0),
(55, '000055', '234', 42, 'SG', 'PENDIENTE', 'files/docs/000055_2024_91062400.pdf', 58, 4, 8, 'SDFS', 'SDF', 23, 'SDF', 32, '', '2024-02-28', '2024-02-28', 0, 0),
(56, '000056', '56', 65, 'FRE', 'PENDIENTE', 'files/docs/000056_2024_91062400.pdf', 58, 4, 8, 'RE', 'DD', 56, 'DGDG', 78, 'HY6', '2024-02-28', '2024-02-28', 0, 0),
(57, '000057', '98', 90, 'FNFG', 'PENDIENTE', 'files/docs/000057_2024_91062400.pdf', 58, 1, 8, 'FGH', 'FGHF', 14, 'FGH', 52, 'HYU7', '2024-02-02', '2024-02-10', 0, 0),
(58, '000058', '234', 5, 'DFSDF', 'PENDIENTE', 'files/docs/000058_2024_91062400.pdf', 58, 2, 8, 'ASF', 'ASF', 3, 'ASF', 3, '454', '2024-03-02', '2024-02-10', 0, 3),
(59, '000059', '234', 2, 'DSD', 'PENDIENTE', 'files/docs/000059_2024_91062400.pdf', 58, 1, 8, 'ASF', 'FASFA', 98, 'SFKKL', 89, 'HEDRE', '2024-03-02', '2024-02-16', 0, 76),
(60, '000060', '2252866', 32, 'CONTRATOS', 'PENDIENTE', 'files/docs/000060_2024_91062400.pdf', 58, 2, 8, 'JURIDICA', 'EVENTUALES', 2, 'CONTRATOS', 3, '', '2022-08-05', '2022-08-26', 32, 258),
(61, '000061', '444', 6, 'HOLA', 'PENDIENTE', 'files/docs/000061_2024_91062400.pdf', 58, 2, 8, 'ASFAS', 'ASDA', 65, 'ASFASF', 56, '454', '2024-03-01', '2024-03-02', 56, 5667),
(62, '000062', '4234', 5235, 'SFSD', 'PENDIENTE', 'files/docs/000062_2024_91062400.pdf', 58, 3, 8, 'SDFGSD', 'SDF', 656, 'SDF', 75, '7575', '2024-01-13', '2024-01-26', 457, 757),
(63, '000063', '6556', 56, 'ASDF', 'PENDIENTE', 'files/docs/000063_2024_91062400.pdf', 58, 3, 8, 'ASFASF', 'SAF', 4, 'ASF', 5, 'ASFAF', '2024-01-05', '2024-01-26', 234, 234),
(64, '000064', '321654', 2, 'ASDASD', 'PENDIENTE', 'files/docs/000064_2024_12345678.pdf', 61, 2, 8, 'ASDAS', 'ASDASD', 10, '123123123', 12312312, '123', '2024-03-01', '2024-03-04', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `idempleado` int(11) NOT NULL,
  `cod_empleado` varchar(15) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `idareainstitu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`idempleado`, `cod_empleado`, `idpersona`, `idareainstitu`) VALUES
(3, 'AMAC000', 1, 16),
(4, 'JCAV002', 8, 8),
(25, '1246484', 47, 1),
(26, '123456789', 48, 12),
(27, '01', 58, 6),
(28, '1234', 60, 1),
(29, '100', 61, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial`
--

CREATE TABLE `historial` (
  `idhistorial` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `expediente` varchar(6) NOT NULL,
  `dni` varchar(8) NOT NULL,
  `accion` varchar(100) NOT NULL,
  `area` varchar(200) NOT NULL,
  `descrip` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `historial`
--

INSERT INTO `historial` (`idhistorial`, `fecha`, `expediente`, `dni`, `accion`, `area`, `descrip`) VALUES
(2, '2022-04-23 13:30:06', '000023', '34534653', 'ACEPTADO', 'SECRETARIA', 'HOLAA'),
(4, '2022-04-23 13:33:04', '000024', '23242134', 'ACEPTADO', 'SECRETARIA', ''),
(5, '2022-04-23 13:34:24', '000026', '23412541', 'ACEPTADO', 'SECRETARIA', ''),
(6, '2022-04-23 13:44:57', 'DERIVA', 'DIRECCIÓ', '', '34534653', 'HOLA PEERO'),
(10, '2022-04-23 14:34:56', '', '', 'RECHAZADO', 'SECRETARíA', 'FALTA FOLIAR'),
(13, '2022-04-23 15:54:37', '000028', '23242134', 'ACEPTADO', 'SECRETARIA', ''),
(14, '2022-04-23 15:54:41', '000029', '23412541', 'ACEPTADO', 'SECRETARIA', ''),
(17, '2022-04-23 21:45:46', '', '', 'RECHAZADO', 'DIRECCIÓN', 'NO CUMPLE CON LAS CONDICIONES'),
(19, '2022-04-23 21:53:22', '000028', '23242134', 'RECHAZADO', 'DIRECCIÓN', 'NO MAMES'),
(26, '2022-04-26 09:10:27', '000031', '15155451', 'ACEPTADO', 'SECRETARIA', 'CORRECTO'),
(28, '2022-04-26 09:12:01', '000031', '15155451', 'ACEPTADO', 'DIRECCIÓN', 'CORRECTO'),
(30, '2022-04-26 09:26:05', '000031', '15155451', 'ACEPTADO', 'RECURSOS HUMANOS', 'ACEPTADO'),
(39, '2022-04-28 18:52:55', '000011', '23412541', 'ACEPTADO', 'ADMINISTRADOR', ''),
(40, '2022-04-28 19:00:37', '000040', '72224693', 'ACEPTADO', 'SECRETARIA', ''),
(42, '2022-04-28 19:01:54', '000040', '72224693', 'ACEPTADO', 'DIRECCIÓN', ''),
(44, '2022-04-28 19:05:05', '000040', '72224693', 'RECHAZADO', 'RECURSOS HUMANOS', ''),
(47, '2022-04-28 22:42:29', '000042', '72224693', 'ACEPTADO', 'SECRETARIA', ''),
(53, '2022-05-21 10:39:15', '000046', '72224693', 'ACEPTADO', 'SECRETARIA', ''),
(54, '2022-05-21 10:42:28', '000044', '70754358', 'ACEPTADO', 'SECRETARIA', ''),
(56, '2022-05-21 10:50:31', '000001', '74185296', 'ACEPTADO', 'OFICINA DE SISTEMAS, INFORMÁTICA Y TELECOMUNICACIONES', ''),
(57, '2022-05-21 10:50:34', '000014', '31415875', 'ACEPTADO', 'OFICINA DE SISTEMAS, INFORMÁTICA Y TELECOMUNICACIONES', ''),
(58, '2022-05-21 10:50:37', '000046', '72224693', 'ACEPTADO', 'OFICINA DE SISTEMAS, INFORMÁTICA Y TELECOMUNICACIONES', ''),
(63, '2024-02-16 10:56:18', '000030', '12341243', 'ACEPTADO', 'SECRETARIA', ''),
(64, '2024-02-16 11:00:24', '000048', '91062400', 'ACEPTADO', 'SECRETARIA', ''),
(66, '2024-02-23 17:15:22', '000049', '', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(67, '2024-02-23 17:24:32', '000049', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(68, '2024-02-23 17:46:15', '000049', '', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(69, '2024-02-23 17:50:14', '000049', '', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(70, '2024-02-23 17:54:32', '000049', '', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(71, '2024-02-23 18:04:40', '000049', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(72, '2024-02-26 10:10:15', '000001', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(73, '2024-02-26 10:59:13', '000001', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(74, '2024-02-26 11:04:03', '000001', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(75, '2024-02-26 11:32:01', '000001', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(76, '2024-02-26 12:04:00', '000001', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(77, '2024-02-26 12:07:24', '000001', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(78, '2024-02-26 12:13:14', '000001', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(79, '2024-02-26 15:09:47', '000001', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(80, '2024-02-27 09:45:09', '000001', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(81, '2024-02-27 09:59:54', '000001', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(82, '2024-02-27 10:47:46', '000001', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(83, '2024-02-27 11:30:35', '000001', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(84, '2024-02-27 12:04:47', '000001', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(85, '2024-02-27 12:08:36', '000001', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(86, '2024-02-27 12:12:39', '000001', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(87, '2024-02-27 12:15:21', '000001', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(88, '2024-02-27 12:25:08', '000001', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(89, '2024-02-27 14:49:18', '000051', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(90, '2024-02-27 15:08:29', '000052', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(91, '2024-02-27 15:52:45', '000053', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(92, '2024-02-28 12:15:07', '000054', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(93, '2024-02-28 15:41:33', '000055', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(94, '2024-02-28 15:47:27', '000055', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(95, '2024-02-28 17:34:02', '000056', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(96, '2024-02-28 18:21:15', '000057', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(97, '2024-02-28 18:24:01', '000057', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(98, '2024-02-28 18:35:16', '000057', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(99, '2024-03-01 15:42:28', '000058', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(100, '2024-03-01 16:15:53', '000059', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(101, '2024-03-01 16:22:18', '000060', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(102, '2024-03-01 17:35:42', '000061', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(103, '2024-03-01 17:43:37', '000062', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(104, '2024-03-01 18:25:41', '000063', '91062400', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(105, '2024-03-04 16:21:06', '000001', '91062400', 'ACEPTADO', 'ARCHIVO CENTRAL', ''),
(106, '2024-03-04 16:23:52', '000001', '91062400', 'DERIVADO', 'UNIDAD DE TECNOLOGIA DE LA INFORMACION (UTI)', 'VERIFIQUE QUE EL DOCUMENTO NO COINCIDE LAS FOJAS'),
(107, '2024-03-04 16:24:55', '000051', '91062400', 'ACEPTADO', 'ARCHIVO CENTRAL', ''),
(108, '2024-03-04 16:27:03', '000051', '91062400', 'DERIVADO', 'UNIDAD DE TECNOLOGIA DE LA INFORMACION (UTI)', 'FOLIOS NO COINCIDEN'),
(109, '2024-03-04 16:27:14', '000052', '91062400', 'ACEPTADO', 'ARCHIVO CENTRAL', ''),
(110, '2024-03-04 16:27:30', '000052', '91062400', 'ARCHIVADO', 'ARCHIVO CENTRAL', 'COMPLETADO'),
(111, '2024-03-04 16:29:40', '000001', '91062400', 'ACEPTADO', 'UNIDAD DE TECNOLOGIA DE LA INFORMACION (UTI)', ''),
(112, '2024-03-04 18:42:54', '000064', '12345678', 'DERIVADO', 'SECRETARÍA', 'INGRESO DE NUEVO TRÁMITE'),
(113, '2024-03-04 18:47:26', '000053', '91062400', 'ACEPTADO', 'ARCHIVO CENTRAL', '6212'),
(114, '2024-03-04 18:48:30', '000053', '91062400', 'DERIVADO', 'ACTIVOS FIJOS', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `institucion`
--

CREATE TABLE `institucion` (
  `idinstitucion` int(11) NOT NULL,
  `ruc` varchar(15) NOT NULL,
  `razon` varchar(200) NOT NULL,
  `dirección` varchar(200) NOT NULL,
  `logo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `institucion`
--

INSERT INTO `institucion` (`idinstitucion`, `ruc`, `razon`, `dirección`, `logo`) VALUES
(1, '200214583453', 'AFCOOP', 'AV. SEñOR DE LOS MILAGROS 043 - BARRIO DE HUAJTACHACRA - ANCASH', 'files/images/inst/logo.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `idpersona` int(11) NOT NULL,
  `dni` varchar(8) NOT NULL,
  `ap_paterno` varchar(100) NOT NULL,
  `ap_materno` varchar(100) NOT NULL,
  `nombres` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `ruc_institu` varchar(15) DEFAULT NULL,
  `institucion` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`idpersona`, `dni`, `ap_paterno`, `ap_materno`, `nombres`, `email`, `telefono`, `direccion`, `ruc_institu`, `institucion`) VALUES
(1, '78945612', 'GUTIERREZ', 'GUERRA', 'YANN', 'adminhacdp@gmail.com', '987654321', 'JR. HUARAZ 520 -  BARRIO DE CONVENTO', NULL, NULL),
(3, '72224693', 'Llallihuaman', 'Giraldo', 'Joel Bladimir', 'joelllalligiraldo@gmail.com', '910959104', 'JR. SAN FRANCISCO S/N - PARCO', NULL, NULL),
(4, '74185296', 'AYALA', 'VILLAVICENCIO', 'GUSTAVO DAVID', 'Maria99@gmail.com', '986714525', 'Jr. Peru 857', NULL, NULL),
(8, '78547854', 'CRUZ', 'B', 'EDWIN', 'jimenm87@gmail.com', '985748574', 'JR PERU', NULL, NULL),
(24, '23242134', 'CORZO', 'ASCENCIO', 'VICTOR MANUEL', 'asfdfsa', '34523425343', 'asdfasfasdf', '', ''),
(25, '34523536', 'LOPEZ', 'AZAÑA', 'MARCIAL', '@GMAIL.COM', '1234567889', 'JR LAS MERCEDES', '', ''),
(26, '12345445', 'JARAMILLO', 'JARAMILLO', 'EDSON', 'EDSON@GMAIL.COM', '1234567890', 'JR LAS MERCEDES', '', ''),
(27, '23412541', 'BERNALDO', 'FERNANDEZ', 'HERMELINDA CARMEN', 'asfdfsa', '1234153534', '435454', '', ''),
(28, '32610292', 'GIRALDO', 'CORZO', 'TERESA', 'techi@gmail.com', '969676430', 'jr san francisco', '', ''),
(30, '31415875', 'GIRALDO', 'CORzO', 'ELIZABETH', 'ELI@GMAIL.COM', '987654321', 'JR. HUAMACHUCO', '', ''),
(31, '87878787', 'ROJO', 'JARAMILLO', 'ALEXANDRA', 'alexandra12@hotmail.com', '12344545456456', 'JR LIMA', '12345455656', 'MUNI POMABAMBA'),
(32, '12341243', 'ADSG', '24234', 'SDGFS', 'Saul@gmail.com', '985555585', 'JR. PERU S/N - BARRIO DE CAñARI', '', ''),
(33, '72224683', 'LLALLIHUAMAN', 'GIRALDO', 'PILAR KATHERINE', 'pilar@pendje.com', '987685968', 'JR HUAMACHUCHO', '', ''),
(34, '12122334', 'FLORES', 'VARGAS', 'SAUL', 'WETTWHYT', '123434567', 'JR HUAMACHUCO', '12345678912', 'POLICIA NACIONAL'),
(35, '23444634', 'EAGTG', 'ZTGTG', '4351431', 'GNSHFHNGFHFH', '564663464356', '5464', '', ''),
(36, '45624525', 'ZDFHGD', 'DGZHDGH', 'FGFSHFG', 'FGHFGH', '45265', 'GHSSG', '', ''),
(37, '14141414', 'QWERTY', 'QWERTY', 'QWERT', 'asdfasd', '1234123423', 'WEER', '', ''),
(38, '14225241', 'FLORES', 'FLORES', 'SAUL', 'asdfasdf', '245235235235', 'AGDFGAFS', '', ''),
(47, '12115441', 'MAMANI', 'C', 'WILFREDO', 'Manuel@gmail.com', '9685869685', 'JR. HUARAZ 755', NULL, NULL),
(48, '78547343', 'VARGAS', 'M', 'BERNARDO', 'cris456@outlook.com', '969676430', 'JR PRIMAVERA', NULL, NULL),
(49, '2456256', 'ADSG', 'ADFGADG', '45DFGADS', 'asdfsadfasdf', '245235235235', 'ASDFASFASDF', '', ''),
(50, '2453645', 'ID=\"IDFILE\"', 'ID=\"IDFILE\"', 'ID=\"IDFILE\"', 'id=\"idfile\"', '34536', 'ID=\"IDFILE\"', '', ''),
(51, '34536345', 'SALYROSAS', 'MARTINEZ', 'YENNY ROSA', 'HOLA', '2342234534543', 'HOLA', '', ''),
(52, '34534653', 'VARGAS', 'ASCENCIA', 'YERALDINE', 'hola pense', '4343453453534', 'JSFRFGREFWR', '', ''),
(53, '15155451', 'CALDAS', 'GUTIERREZ', 'MARYORI', 'maritza@gmail.com', '985858447', 'JR. LAS AMERICAS S/N', '', ''),
(54, '54454554', 'MONTERO', 'CARDONA', 'SAUL', 'saul@mendoza.unasam.edu.pe', '234252453', 'BARRIO DE PARCO', '', ''),
(55, '70754358', 'MATIAS', 'ROJAS', 'ZAIDY', 'starzaidy21@gmail.com', '987654321', 'SHILLA', '', ''),
(58, '91062400', 'VELASQUEZ', 'FERREL', 'YECID JUNIOR', 'velasquezyecid@gmail.com', '75284119', 'VILLA LITORAL N.220', NULL, NULL),
(59, '', '', '', '', '', '', '', '', ''),
(60, '10101010', 'CALLE', 'VELAZQUES', 'MARY', 'mary@gmail.com', '77505050', 'AV. BUSH', NULL, NULL),
(61, '12345678', 'PEREZ', 'PEREZ', 'YANN', 'w@afcoop.gob.bo', '71111111', 'C/ASDASD', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `idroles` int(11) NOT NULL,
  `rol` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`idroles`, `rol`) VALUES
(1, 'Administrador'),
(9, 'Colaborador'),
(10, 'Funcionario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodoc`
--

CREATE TABLE `tipodoc` (
  `idtipodoc` int(11) NOT NULL,
  `tipodoc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `tipodoc`
--

INSERT INTO `tipodoc` (`idtipodoc`, `tipodoc`) VALUES
(1, 'ARCHIVO 1'),
(2, 'ARCHIVO 2'),
(3, 'ARCHIVO 3'),
(4, 'ARCHIVO 4');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `dni` varchar(8) NOT NULL,
  `contraseña` varchar(150) NOT NULL,
  `email` varchar(100) NOT NULL,
  `fecharegistro` datetime NOT NULL,
  `ultacceso` datetime DEFAULT NULL,
  `fechaedicion` datetime NOT NULL,
  `estado` varchar(50) NOT NULL,
  `foto` text DEFAULT NULL,
  `idroles` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuarios`, `nombre`, `dni`, `contraseña`, `email`, `fecharegistro`, `ultacceso`, `fechaedicion`, `estado`, `foto`, `idroles`) VALUES
(1, 'Admin', '78945612', '123456', 'aadminhacdp@gmail.com', '2022-03-20 12:17:23', '2022-03-20 12:17:23', '2024-02-20 16:28:43', 'ACTIVO', 'files/images/1/logo.png', 1),
(5, 'ECRUZ', '78547854', '123456789', 'ajimenm87@gmail.com', '2022-03-27 16:12:59', NULL, '2024-02-20 16:29:17', 'ACTIVO', 'files/images/5/5_2022_78547854.jpg', 9),
(33, 'WMAMANI', '12115441', 'manuel', 'aManuel@gmail.com', '2022-04-21 17:36:56', NULL, '2024-02-20 16:29:35', 'ACTIVO', 'files/images/33/33_2022_12115441.jpg', 10),
(34, 'BVARGAS', '78547343', '123', 'acris456@outlook.com', '2022-04-21 18:18:01', NULL, '2024-02-20 17:01:21', 'ACTIVO', 'files/images/34/34_2022_78547343.jpg', 9),
(37, '91062400', '91062400', 'Ra9106240', 'velasquezyecid@gmail.com', '2024-02-16 10:06:39', NULL, '2024-03-01 18:25:40', 'ACTIVO', 'files/images/0/persona.png', 10),
(38, 'mary', '10101010', '123456', 'mary@gmail.com', '2024-03-04 17:44:16', NULL, '2024-03-04 17:51:25', 'ACTIVO', 'files/images/38/38_2024_10101010.jpg', 9),
(39, 'w@afcoop.gob.bo', '12345678', '123456', 'w@afcoop.gob.bo', '2024-03-04 18:27:55', NULL, '2024-03-04 18:42:54', 'ACTIVO', 'files/images/0/persona.png', 10);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`idarea`);

--
-- Indices de la tabla `areainstitu`
--
ALTER TABLE `areainstitu`
  ADD PRIMARY KEY (`idareainstitu`),
  ADD KEY `idinstitucion` (`idinstitucion`),
  ADD KEY `idarea` (`idarea`);

--
-- Indices de la tabla `derivacion`
--
ALTER TABLE `derivacion`
  ADD PRIMARY KEY (`idderivacion`),
  ADD KEY `idareainstitu` (`idareainstitu`),
  ADD KEY `iddocumento` (`iddocumento`);

--
-- Indices de la tabla `documentacion`
--
ALTER TABLE `documentacion`
  ADD PRIMARY KEY (`id_orden`);

--
-- Indices de la tabla `documento`
--
ALTER TABLE `documento`
  ADD PRIMARY KEY (`iddocumento`),
  ADD KEY `idpersona` (`idpersona`),
  ADD KEY `idtipodoc` (`idtipodoc`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`idempleado`),
  ADD KEY `idpersona` (`idpersona`),
  ADD KEY `idareainstitu` (`idareainstitu`);

--
-- Indices de la tabla `historial`
--
ALTER TABLE `historial`
  ADD PRIMARY KEY (`idhistorial`);

--
-- Indices de la tabla `institucion`
--
ALTER TABLE `institucion`
  ADD PRIMARY KEY (`idinstitucion`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idpersona`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`idroles`);

--
-- Indices de la tabla `tipodoc`
--
ALTER TABLE `tipodoc`
  ADD PRIMARY KEY (`idtipodoc`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuarios`),
  ADD KEY `idroles` (`idroles`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `area`
--
ALTER TABLE `area`
  MODIFY `idarea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `areainstitu`
--
ALTER TABLE `areainstitu`
  MODIFY `idareainstitu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `derivacion`
--
ALTER TABLE `derivacion`
  MODIFY `idderivacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT de la tabla `documentacion`
--
ALTER TABLE `documentacion`
  MODIFY `id_orden` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `documento`
--
ALTER TABLE `documento`
  MODIFY `iddocumento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `idempleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `historial`
--
ALTER TABLE `historial`
  MODIFY `idhistorial` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT de la tabla `institucion`
--
ALTER TABLE `institucion`
  MODIFY `idinstitucion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `idpersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `idroles` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `tipodoc`
--
ALTER TABLE `tipodoc`
  MODIFY `idtipodoc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuarios` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `areainstitu`
--
ALTER TABLE `areainstitu`
  ADD CONSTRAINT `areainstitu_ibfk_1` FOREIGN KEY (`idinstitucion`) REFERENCES `institucion` (`idinstitucion`),
  ADD CONSTRAINT `areainstitu_ibfk_2` FOREIGN KEY (`idarea`) REFERENCES `area` (`idarea`);

--
-- Filtros para la tabla `derivacion`
--
ALTER TABLE `derivacion`
  ADD CONSTRAINT `derivacion_ibfk_1` FOREIGN KEY (`idareainstitu`) REFERENCES `areainstitu` (`idareainstitu`),
  ADD CONSTRAINT `derivacion_ibfk_2` FOREIGN KEY (`iddocumento`) REFERENCES `documento` (`iddocumento`);

--
-- Filtros para la tabla `documento`
--
ALTER TABLE `documento`
  ADD CONSTRAINT `documento_ibfk_1` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`idpersona`),
  ADD CONSTRAINT `documento_ibfk_2` FOREIGN KEY (`idtipodoc`) REFERENCES `tipodoc` (`idtipodoc`);

--
-- Filtros para la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD CONSTRAINT `empleado_ibfk_1` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`idpersona`),
  ADD CONSTRAINT `empleado_ibfk_2` FOREIGN KEY (`idareainstitu`) REFERENCES `areainstitu` (`idareainstitu`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`idroles`) REFERENCES `roles` (`idroles`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
