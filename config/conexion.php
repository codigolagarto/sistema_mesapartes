<?php
session_start();

class Conectar {
    protected $dbh;

    protected function Conexion(){
        try {
            // Utilizamos variables para definir la conexión a la base de datos
            $db_host = "localhost"; // Puedes cambiar esto si tu base de datos está en otro servidor
            $db_name = "db_hacdp";
            $db_user = "root";
            $db_pass = "";

            $conectar = $this->dbh = new PDO("mysql:host={$db_host};dbname={$db_name}", $db_user, $db_pass);

            return $conectar;    
        } catch (Exception $e) {
            print "¡Error BD!: " . $e->getMessage() . "<br/>";
            die();  
        }
    }
    
    public function set_names(){    
        return $this->dbh->query("SET NAMES 'utf8'");
    }

    // Función para obtener la URL base de la aplicación
    public function ruta(){
        // Obtener el protocolo (http o https)
        $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http";
        
        // Obtener el nombre del host
        $host = $_SERVER['HTTP_HOST'];

        // Obtener la ruta base
        $url_base = "{$protocol}://{$host}/Sistema_MesaPartes/";

        return $url_base;
    }
}
?>
