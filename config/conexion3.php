<?php

$host = "192.168.0.10"; 
$port = "5432"; 
$dbname = "rec_v2_prueba"; 
$user = "postgres"; 
$password = "rootpostgres"; 

try {
    // Crear una instancia de la clase PDO para establecer la conexión
    $conexion_pg = new PDO("pgsql:host=$host;port=$port;dbname=$dbname;user=$user;password=$password");
    
    // Configurar el modo de manejo de errores para lanzar excepciones
    $conexion_pg->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    // Mensaje de éxito si la conexión se estableció correctamente
    //echo "Conexión exitosa a la base de datos PostgreSQL";
} catch (PDOException $e) {
    // Capturar y mostrar cualquier excepción
    echo "Error de conexión: " . $e->getMessage();
}
?>
