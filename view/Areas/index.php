
<?php 
 include("../../config/conexion.php");
 include("../../config/conexion2.php");
if (!isset($_SESSION["idusuarios"])) {
  header("Location: /Sistema_MesaPartes/Acceso/");
}
$iduser=$_SESSION["idusuarios"];
$foto=$_SESSION["foto"];
$dni=$_SESSION["dni"];

$consulta=mysqli_query($conexion,"select idinstitucion, area from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$area = mysqli_fetch_assoc($consulta);

$consulta1=mysqli_query($conexion,"select idinstitucion, ae.idarea ID, area from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$area1 = mysqli_fetch_assoc($consulta1);

$institucion=mysqli_query($conexion,"select * from institucion where idinstitucion='1'");
$row = mysqli_fetch_assoc($institucion);

$institucion1=mysqli_query($conexion,"select * from institucion");
?>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<?php
include '../includes/scripts.php'; //nos permite visualizar 
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
<!-- INICIO DEL CONTENIDO PRINCIPAL-->

<?php
	include '../includes/header.php'; //nos permite visualizar 
?>

<!-- Main content modificar -->
<body class="hold-transition sidebar-mini layout-fixed">

<!-- ************************** GENERAL *********************************** -->

<!-- MODAL INGRESO Y EDICION DE AREAS-->
<div class="modal fade" id="modalarea">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" id="modal-header">
            <h4 style="font-weight:600;color:#1F618D" class="modal-title" id="modal-title">GESTIÓN DE ÁREAS</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
            </button>
            </div>
            <div class="modal-body">
                <form id="formarea">
                        
                            <input type="hidden" class="form-control" name="idid" id="idid">
                        
                        <div class="form-group">
                            <label for="inputName">Código</label>
                            <input type="text" class="form-control" name="icod" id="icod" onkeypress="return validaNumericos(event)">
                        </div>
                        <div class="form-group">
                            <label for="inputName">Área</label>
                            <input type="text" class="form-control" name="iarea" id="iarea">
                        </div>  
                        <div class="form-group">
                            <label>Institución</label> &nbsp;
                            <a style="width:20px;height:30px"></a>
                            <select class="form-control" name="tipoinsti" id="tipoinsti">
                                <?php while($datos=mysqli_fetch_array($institucion1)) {?>
                                    <option value="<?php echo $datos['idinstitucion']  ?>"> 
                                    <?php echo $datos['razon'] ?></optiomn>
                                <?php }?>
                            </select> 
                        </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
            <button style="height: 40px;width: 120px;" type="button" class="btn btn-danger" data-dismiss="modal" onclick="limpiarcamposarea()">Cancelar</button>
            <button style="height: 40px;width: 120px;" type="button" class="btn btn-primary" id="editara">Editar</button>
            <button style="height: 40px;width: 120px;" type="button" class="btn btn-primary" id="guardara">Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header">
      <ul class="navbar-nav">
       
        <li class="nav-item">
        <input id="idareaid" name="idarealogin" type="hidden" value="<?php echo $area1['ID'];?>">
        <input id="idarealogin" name="idarealogin" type="hidden" value="<?php echo $area1['area'];?>">
        <input id="idinstitu" name="idinstitu" type="hidden" value="<?php echo $area1['idinstitucion'];?>">
       
        </li>
        <!-- <a class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fas fa-search"><span style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;font-weight: 500;" > Buscar</span></i>
        </a> -->
      </ul>
      
    </nav>
    <!-- /.navbar -->
  
  </div>
<!-- Main content -->
<div class="wrapper">
 <!-- Main content -->
 <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">

              <div class="card card-danger card-outline">
                <div class="card-header">
                  <h3 class="card-title" style="font-weight:600; color:#084B8A">Áreas Registradas</h3>
                  <a style="float:right;width:220px;height:30px" class="btn btn-flat bg-success" data-toggle="modal" id="Nuevoa">
                    <i class="nav-icon fas fa-plus"></i>&nbsp;&nbsp;Nuevo Registro </a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                <a style="float:right;width:220px;height:30px;" Target="_blank" class="btn btn-flat bg-gray-dark" href="../../reporte/reporte-areas.php" id="ReportUsu">
                    <i class="nav-iconfas fas fa-file-pdf"></i>&nbsp;&nbsp;Generar Reporte </a>
                <table id="tablaAreas" class="tabla_t" style="width:100%" >
                <thead class="tabla_th">
                      <tr>
                        <th >ID</th>
                        <th>Código</th>  
                        <th>Área</th>
                        <th>Acción</th>
                      </tr>
                    </thead>
                    <tbody class="tabla_tb">                           
                    
                    </tbody>
              
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
 
    </div>  
        <!-- /.container-fluid -->
      </section>
      <!-- /.content -->

</div>



<script src="/Sistema_MesaPartes/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
</html>

<script>
$( function() {
    $( "#datepicker" ).datepicker(
$.datepicker.regional[ "es" ]
    );
    
} );

</script>
<!-- FIN  DEL CONTENIDO PRINCIPAL -->