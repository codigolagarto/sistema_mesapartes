<?php
include("../../config/conexion.php");
require_once("../../config/conexion2.php");
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_FILES['idfile10'])) {

    $name = $_POST['idunidad'];
    $codigo_referencia = $_POST['idcodrefe'];
    // Otros campos del formulario...
    
    // Carpeta de destino para guardar el archivo
    $targetDir =  "../../";
    $ruta = "files/docs/";    
    // Nombre de archivo aleatorio
    $fileName = uniqid() . '_' . basename($_FILES['idfile10']['name']);
    $targetFile = $targetDir.$ruta. $fileName;

    // Guardar el archivo en la carpeta de destino
    if (move_uploaded_file($_FILES['idfile10']['tmp_name'], $targetFile)) {
        echo "El archivo $fileName ha sido subido correctamente. Nombre: $targetFile";
    } else {
        echo "Error al subir el archivo.";
    }
} else {
    echo "No se ha recibido ningún archivo.";

}
exit;
?>