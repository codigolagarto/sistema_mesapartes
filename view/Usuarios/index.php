
<?php 
include("../../config/conexion.php");
include("../../config/conexion2.php");
if (!isset($_SESSION["idusuarios"])) {
  header("Location: /Sistema_MesaPartes/Acceso/");
}
$iduser=$_SESSION["idusuarios"];
$foto=$_SESSION["foto"];
$dni=$_SESSION["dni"];

$consulta=mysqli_query($conexion,"select idinstitucion, area from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$area = mysqli_fetch_assoc($consulta);

$consulta1=mysqli_query($conexion,"select idinstitucion, ae.idarea ID, area from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$area1 = mysqli_fetch_assoc($consulta1);

$institucion=mysqli_query($conexion,"select * from institucion where idinstitucion='1'");
$row = mysqli_fetch_assoc($institucion);

$query1=mysqli_query($conexion,"SELECT * FROM roles");

$query2=mysqli_query($conexion,"SELECT * FROM roles");
?>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<?php
include '../includes/scripts.php'; //nos permite visualizar 
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
<!-- INICIO DEL CONTENIDO PRINCIPAL-->

<?php
	include '../includes/header.php'; //nos permite visualizar 
?>

<!-- Main content modificar -->
<body class="hold-transition sidebar-mini layout-fixed">

<!-- ************************** GENERAL *********************************** -->

  <!-- MODAL INGRESO DE USUARIO-->
  <div class="modal fade" id="modalusuario">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header btn-primary" id="modal-header">
              <h4 style="font-weight:600" class="modal-title" id="modal-title"><i class="bi bi-person-add"></i>GESTIÓN DE USUARIOS </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
              </button>
            </div>
            <div class="modal-body">
            <form id="formnew">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputName">CI</label>
                            <input type="text" class="form-control" name="idni" id="idni" onkeypress='return validaNumericos(event)' maxlength="8" minlength="8">
                            <b id="Aviso"></b>
                          </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputName">Nombres</label>
                            <input type="text" class="form-control" name="inombre" id="inombre">
                        </div>
                      </div>

                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="inputName">Apellido Paterno</label>
                              <input type="text" class="form-control" name="iappat" id="iappat">
                          </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputName">Apellido Materno</label>
                            <input type="text" class="form-control" name="iapmat" id="iapmat">
                        </div>
                      </div>
                    </div>

                                                      

                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputEmail">Celular</label>
                            <input type="text" class="form-control"  name="icel" id="icel">
                        </div>
                      </div>
                      
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputEmail">Dirección</label>
                            <input type="text" class="form-control"  name="idir" id="idir">
                        </div>
                      </div> 
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputMessage">Email</label>
                            <input type="email" class="form-control1"  name="iemail" id="iemail">
                            <b id="AvisoE"></b>
                        </div>
                      </div>  
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputEmail">Nombre Usuario</label>
                            <input type="text" class="form-control1"  name="inomusu"  id="inomusu">
                            <b id="error1"></b>
                        </div>
                      </div>                   
                    </div>
            
                    <div class="row">
                  
                      <div class="col-sm-6">
                          <div class="form-group">
                            <label>Rol</label> &nbsp;
                            
                            <select class="form-control" name="tipo" id="tipo"><?php while($datos=mysqli_fetch_array($query1)) {?>
                                    <option value="<?php echo $datos['idroles']  ?>"> <?php echo $datos['rol'] ?></option>
                                <?php }?>
                            </select> 
                            
                          </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label>Contraseña</label>
                            <input type="password" class="form-control"  name="ipasss" id="ipasss"/>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group">
                            <label for="inputEmail">Confirmar Contraseña</label>
                            <input type="password" class="form-control"  name="ipassco" id="ipassco"/>
                            <p id="error2"></p>
                        </div>
                      </div>
                    </div>
            </form>
            </div>
            <div class="modal-footer justify-content-between">
              <button style="height: 40px;width: 120px;" type="button" class="btn btn-danger" data-dismiss="modal" onclick="limpiarcampos()">Cancelar</button>
              <button style="height: 40px;width: 120px;" type="button" class="btn btn-primary" id="guardar">Registrar</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

  <!-- MODAL EDICIÓN DE USUARIO-->
  <div class="modal fade" id="modalEdusuario">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header btn-primary" id="modal-header">
              <h4 style="font-weight:600" class="modal-title" id="modal-title"><i class="bi bi-person-badge"></i>EDICIÓN DE USUARIOS</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
              </button>
            </div>
            <div class="modal-body">
            <form id="formEdit">
                    <input type="hidden" class="form-control" name="idusu" id="idusu">
                    <input type="hidden" class="form-control" name="idper" id="idper">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputName">CI</label>
                            <input type="text" class="form-control" name="idni" id="idni1" onkeypress='return validaNumericos(event)' maxlength="8" minlength="8">
                            <b id="Aviso"></b>
                          </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputName">Nombres</label>
                            <input type="text" class="form-control" name="inombre" id="inombre1">
                        </div>
                      </div>
                    </div>

                     <div class="row">
                       <div class="col-sm-6">
                          <div class="form-group">
                              <label for="inputName">Apellido Paterno</label>
                              <input type="text" class="form-control" name="iappat" id="iappat1">
                          </div>
                       </div>
                       
                       <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputName">Apellido Materno</label>
                            <input type="text" class="form-control" name="iapmat" id="iapmat1">
                        </div>
                      </div>
                     </div>                                        

                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputEmail">Celular</label>
                            <input type="text" class="form-control"  name="icel" id="icel1">
                        </div>
                      </div>
                      
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputEmail">Dirección</label>
                            <input type="text" class="form-control"  name="idir" id="idir1">
                        </div>
                      </div>                      
                    </div>
                    
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputMessage">Email</label>
                            <input type="email" class="form-control1"  name="iemail" id="iemail1">
                            <b id="AvisoE"></b>
                        </div>
                      </div>
                      
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputEmail">Nombre Usuario</label>
                            <input type="text" class="form-control1"  name="inomusu"  id="inomusu1">
                            <b id="error1"></b>
                        </div>
                      </div>
                    </div>

                    <div  class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label>Rol</label> &nbsp;
                      
                            <select class="form-control" name="tipo" id="tipo1"><?php while($datos=mysqli_fetch_array($query2)) {?>
                                    <option value="<?php echo $datos['idroles']?>"> <?php echo $datos['rol']?></option>
                                <?php }?>
                            </select> 
                          </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label>Estado</label> &nbsp;
                            <select class="form-control" name="estado1" id="estado1">
                                    <option value="ACTIVO">ACTIVO</option>
                                    <option value="DESACTIVADO">DESACTIVADO</option>
                            </select> 
                          </div>
                      </div>
                    </div>
            </form>
            </div>
            <div class="modal-footer justify-content-between">
              <button style="height: 40px;width: 120px;" type="button" class="btn btn-danger" data-dismiss="modal" onclick="limpiarcampos()">Cancelar</button>
              <button style="height: 40px;width: 120px;" type="button" class="btn btn-primary" id="Editar">Editar</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
  </div>
  
<!-- MODAL CAMBIO DE CONTRASEÑA-->
<div class="modal fade" id="modaleditpsw1"  >
   <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 style="font-weight:600" class="modal-title">CAMBIO DE CONTRASEÑA:</h4>
          &nbsp;<b id="idc" style="color:#8C0505;font-size: 1.4rem;"></b> 
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">  
          <form id="formC">
            <div class="form-group">
                <label>Contraseña Actual</label>
                <input type="password" class="form-control1" name="ipsw" id="ipsw"/>
            </div><div class="form-group">
                <label>Contraseña Nueva</label>
                <input type="password" class="form-control1" name="ipasss1" id="ipasss1"/>
            </div><div class="form-group">
                <label>Confirmar nueva contraseña</label>
                <input type="password" class="form-control1" name="ipassco1" id="ipassco1"/>
                <b id="error3"></b>
            </div> 
          </form> 
        </div>
        <div class="modal-footer justify-content-between">
          <button style="height:40px;width:120px" type="button" class="btn btn-danger" data-dismiss="modal" id="SalirC">Cancelar </button>
          <button style="height:40px;width:120px" type="button" class="btn btn-primary" id="BtnContra">Actualizar</button>
        </div>
      </div>
    </div>
</div>
                                 
  <!-- MODAL FOTO-->
  <div class="modal fade" id="modalfoto"  >
   <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 style="font-weight:600" class="modal-title">ACTUALIZAR FOTO DE PERFIL:</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form id="FormFoto">
        <div style="text-align:center;" class="modal-body">
          <h1 style="font-family:arial;font-size:20px;font-weight:600">Foto de perfil Actual</h1>  
          <img style="widht: 150px; height:150px;" id="FotoP" name="FotoP">
          <br><br>
          <div class="form-group">
              <label>Elegir Foto (jpg)</label><span style="color: red;font-weight: 600;"> (*)</span>
              <div class="file">
                  <input type="hidden" id="opcion" name="opcion" value='10'>
                  <input type="hidden" id="iddni1" name="iddni1">
                  <input type="hidden" id="idusua" name="idusua">
                  <input type="file" id="idfile1" name="idfile1" required accept=".jpg">
              </div>
    
          </div>

        </div>
        <div class="modal-footer justify-content-between">
          <button style="height:40px;width:120px" type="button" class="btn btn-danger" data-dismiss="modal">Cancelar </button>
          <button style="height:40px;width:120px" type="submit" class="btn btn-primary" id="CambiarF">Cambiar</button>
        </div>
        </form>
      </div>
    </div>
</div>

  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header">
      <ul class="navbar-nav">
    
        <li class="nav-item">
        <input id="idareaid" name="idarealogin" type="hidden" value="<?php echo $area1['ID'];?>">
        <input id="idarealogin" name="idarealogin" type="hidden" value="<?php echo $area1['area'];?>">
        <input id="idinstitu" name="idinstitu" type="hidden" value="<?php echo $area1['idinstitucion'];?>">
        </li>
        <!-- <a class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fas fa-search"><span style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;font-weight: 500;" > Buscar</span></i>
        </a> -->
      </ul>
      
    </nav>
    <!-- /.navbar -->
  

<!-- Main content -->

<section class="content">
        <div class="container-fluid">
        <div class="row">
            <div class="col-12">

            <div class="card card-danger card-outline">
                <div class="card-header">
                <h3 class="card-title" style="font-weight:600; color:#084B8A">Usuarios Registrados</h3>
                <a style="float:right;width:220px;height:30px" class="btn btn-flat bg-success" data-toggle="modal" id="Nuevo">
                    <i class="nav-icon fas fa-plus"></i>&nbsp;&nbsp;Nuevo Registro </a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                <a style="float:right;width:220px;height:30px;" Target="_blank" class="btn btn-flat bg-gray-dark" href="../../reporte/reporte-usuario.php" id="ReportUsu">
                    <i class="nav-iconfas fas fa-file-pdf"></i>&nbsp;&nbsp;Generar Reporte </a>
                <table id="tablaUsuarios" class="tabla_t" style="width:100%;">
                <thead class="tabla_th">
                    <tr>
                        <th>ID</th>
                        <th>Usuario</th>  
                        <th>CI</th>
                        <th>Email</th>
                        <th>Estado</th>
                        <th >Foto</th>
                        <th>Acción</th>
                    </tr>
                    </thead>
                    <tbody class="tabla_tb">                           
                    </tbody>        
            </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </div>
</div>  
        <!-- /.container-fluid -->
    </section>

                            </div>

    <script src="/Sistema_MesaPartes/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
</html>

<script>
$( function() {
    $( "#datepicker" ).datepicker(
$.datepicker.regional[ "es" ]
    );
    
} );

</script>
<!-- FIN  DEL CONTENIDO PRINCIPAL -->