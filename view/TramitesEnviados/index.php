
<?php 
 include("../../config/conexion.php");
 include("../../config/conexion2.php");
if (!isset($_SESSION["idusuarios"])) {
  header("Location: /Sistema_MesaPartes/Acceso/");
}
$iduser=$_SESSION["idusuarios"];
$dni=$_SESSION["dni"];
$foto=$_SESSION["foto"];
$dni=$_SESSION["dni"];

$consulta=mysqli_query($conexion,"select idinstitucion, area from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$area1 = mysqli_fetch_assoc($consulta);

$institucion=mysqli_query($conexion,"select * from institucion where idinstitucion='1'");
$row = mysqli_fetch_assoc($institucion);

$institucion1=mysqli_query($conexion,"select * from institucion");

$consulta=mysqli_query($conexion,"select a.idarea ID,area from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$resultado = mysqli_fetch_assoc($consulta);

$area_actual = $resultado['area'];


$area=mysqli_query($conexion,"select a.idarea ID, cod_area, area from institucion i, area a, areainstitu ae where ae.idinstitucion=i.idinstitucion and ae.idarea=a.idarea and area!='$area_actual'");

$consulta1=mysqli_query($conexion,"select date_format();");
$resultado1 = mysqli_fetch_assoc($consulta) 


?>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<?php
include '../includes/scripts.php'; //nos permite visualizar 
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
<!-- INICIO DEL CONTENIDO PRINCIPAL-->

<?php
	include '../includes/header.php'; //nos permite visualizar 
?>

<!-- Main content modificar -->
<body class="hold-transition sidebar-mini layout-fixed">

<!-- MODAL DE ACEPTACION-->
<div class="modal fade" id="modalacept">
        <div class="modal-dialog modal-lg modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header" id="modal-header">
              <h4 class="modal-title" style="font-weight:600" id="modal-title">ACEPTAR/RECHAZAR DOCUMENTO</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="Formaceptacion">
                
                  <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                              <input type="hidden" class="form-control" name="iddoc1" id="iddoc1">
                                  
                                <label>N° Documento </label><span style="color: red;font-weight: 600;">(*)</span>
                                <input style="font-weight:600;font-size:18px" type="text" class="form-control" id="inrodoc1" disabled name="inrodoc1">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>N° Folios </label><span style="color: red;font-weight: 600;">(*)</span>
                                <input style="font-weight:600;font-size:18px" type="number" class="form-control" id="ifolio1" disabled name="ifolio1">
                            </div>
                        </div>                            
                  </div>
                  <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>N° Expediente </label><span style="color: red;font-weight: 600;">(*)</span>
                                <input style="font-weight:600;font-size:18px" type="text" class="form-control" id="iexpediente1" disabled name="iexpediente1">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Estado </label><span style="color: red;font-weight: 600;">(*)</span>
                                <input style="font-weight:600;font-size:18px" type="text" class="form-control" id="iestad1" disabled name="iestad1">
                            </div>
                        </div>                           
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                          <label>CI</label><span style="color: red;font-weight: 600;"> (*)</span>
                          <input style="font-weight:600;font-size:18px" type="number" class="form-control" id="idni1" disabled name="idni1">
                      </div>     
                    </div> 
                      <div class="col-sm-6">
                          <div class="form-group">
                            <label>Asunto </label><span style="color: red;font-weight: 600;">(*)</span>
                            <input style="font-weight:600;font-size:18px" class="form-control"  id="iasunt1" disable name="iasunt1">
                          </div>
                        </div>  
                        <div class="col-sm-6">
                      <div class="form-group">
                          <label>Fondo</label><span style="color: red;font-weight: 600;"> (*)</span>
                          <input style="font-weight:600;font-size:18px" type="text" class="form-control" id="itipodoc1" disabled name="itipodoc1">
                      </div>     
                    </div> 
                    <div class="col-sm-6">
                      <div class="form-group">
                          <label>Seccion</label><span style="color: red;font-weight: 600;"> (*)</span>
                          <input style="font-weight:600;font-size:18px" type="text" class="form-control" id="itipodoc1" disabled name="itipodoc1">
                      </div>     
                    </div>                          
                  </div>
                  <div class="row"> 
                      <div class="col-sm-12">
                          <div class="form-group">
                            <label>Descripción: </label><span style="color: #BDBCBC;font-weight: 600;">(Opcional)</span>
                            <textarea class="form-control" rows="3" id="des" name="des" placeholder="Ingrese la descripción..."></textarea>
                          </div>
                        </div>                           
                  </div>
                
              </form>
            </div>
            <div class="modal-footer justify-content-between">
              <button style="float:left;height: 40px;width: 120px;" type="button" class="btn btn-primary" data-dismiss="modal" id="btnCerra">Cerrar</button>
              <a></a><a></a><a></a><a></a><a></a><a></a><a></a><a></a>
              <button style="float:right;height: 40px;width: 120px;" type="button" class="btn btn-success" id="btnAcepta">Aceptar</button>
              <button style="float:right;height: 40px;width: 120px;" type="button" class="btn btn-danger" id="btnRechazar">Rechazar</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
</div>

     <!-- MODAL MAS INFORMACION-->
     <div class="modal fade" id="modalmas">
        <div class="modal-dialog modal-lg modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header" id="modal-header">
              <h4 class="modal-title color_t" style="font-weight:600 " id="modal-title">Datos del Documento</h4>
              <div class=".col-md-4 .ms-auto">
                <a style="height:40px;width:120px;font-weight:600;padding:6px" id="iddocumento" href="#" class="btn btn-primary">Documento</a>
                <a style="height:40px;width:120px;font-weight:600;padding:6px" id="idremitent" href="#" class="btn btn-light">Tramite</a>
                <a style="height:40px;width:120px;font-weight:600;padding:6px" id="idvistapre" href="#" class="btn btn-light">Vista previa</a>
              </div>
            </div>
            <div class="modal-body">
              <form>
                <div id="tramite1">
                  <div class="row">
                        <div class="col-sm-3 color_t"><b>N° Documento</b>
                            
                              <input type="hidden" class="form-control" name="iddoc" id="iddoc">
              
                              <input type="text" class="step__input" id="inrodoc" disabled name="inrodoc">
                          
                        </div>
                        <div class="col-sm-4 color_t"><b>Remitente</b>
                        <input type="text" id="idremi" name="idremi" placeholder="Fondo" class="step__input" disabled>
                        </div>  
                        <div class="col-sm-5 color_t"><b>Fondo</b>
                        <input type="text" id="idfondo" name="idfondo" placeholder="Fondo" class="step__input" disabled>
                        </div>  
                  </div>
                  <div class="row">
                        <div class="col-sm-3 color_t"><b>Sub Sección</b>
                      
                              <input type="text" class="step__input" id="subsec" disabled name="subsec">
                          
                        </div>
                        <div class="col-sm-4 color_t"><b>Sección</b>
                        <input type="text" id="seccion" name="seccion"  class="step__input" disabled>
                        </div>  
                        <div class="col-sm-5 color_t"><b>Serie Documental</b>
                        <input type="text" id="serie" name="serie"  class="step__input" disabled>
                        </div>  
                  </div>
                  <div class="row">
                        <div class="col-sm-3 color_t"><b>Sub Serie</b>
                      
                              <input type="text" class="step__input" id="subserie" disabled name="subserie">
                          
                        </div>
                        <div class="col-sm-4 color_t"><b>Código de Ref.</b>
                        <input type="text" id="codigo" name="codigo"  class="step__input" disabled>
                        </div>  
                        <div class="col-sm-5 color_t"><b>Fechas extremas</b>
                        <input type="text" id="fechas" name="fechas"  class="step__input" disabled>
                        </div>  
                  </div>
                  <div class="row">
                        <div class="col-sm-3 color_t"><b>Cantidad de Unidades</b>
                      
                              <input type="text" class="step__input" id="cantidad" disabled name="cantidad">
                          
                        </div>
                        <div class="col-sm-4 color_t"><b>Extensión en Metros</b>
                        <input type="text" id="metros" name="metros"  class="step__input" disabled>
                        </div>  
                        <div class="col-sm-5 color_t"><b>Estado</b>
                        <input type="text" id="estado" name="estado"  class="step__input" disabled>
                        </div>  
                  </div>
                 
                </div>

                <div id="remitente1">
                <div class="row">
                        <div class="col-sm-3 color_t"><b>Código de Ref.</b>
                      
                              <input type="text" class="step__input" id="codigos" disabled name="codigos">
                          
                        </div>
                        <div class="col-sm-4 color_t"><b>N° de Caja</b>
                        <input type="text" id="caja" name="caja"  class="step__input" disabled>
                        </div>  
                        <div class="col-sm-5 color_t"><b>Título del documento</b>
                        <input type="text" id="titulo" name="titulo"  class="step__input" disabled>
                        </div>  
                  </div>

                  <div class="row">
                        <div class="col-sm-3 color_t"><b>Fechas Extremas</b>
                      
                              <input type="text" class="step__input" id="fechass" disabled name="fechass">
                          
                        </div>
                        <div class="col-sm-4 color_t"><b>Tomo/Volumen</b>
                        <input type="text" id="tomo" name="tomo"  class="step__input" disabled>
                        </div>  
                        <div class="col-sm-5 color_t"><b>Fojas</b>
                        <input type="text" id="fojas" name="fojas"  class="step__input" disabled>
                        </div>  
                  </div>
                  <div class="row">
                        <div class="col-sm-3 color_t"><b>Soporte Técnico</b>
                      
                              <input type="text" class="step__input" id="soporte" disabled name="soporte">
                        </div>
                        <div class="col-sm-4 color_t"><b>Nombre de la Unidad</b>
                        <input type="text" id="unidad" name="unidad"  class="step__input" disabled>
                        </div>  
                        <div class="col-sm-2 color_t"><b>Ambiente</b>
                        <input type="text" id="ambiente" name="ambiente"  class="step__input" disabled>
                        </div>  
                        <div class="col-sm-3 color_t"><b>Estante</b>
                        <input type="text" id="estante" name="estante"  class="step__input" disabled>
                        </div>  
                  </div>

                  <div class="row">
                  <div class="col-sm-2 color_t"><b>Balda</b>
                        <input type="text" id="balda" name="balda"  class="step__input" disabled>
                        </div>  
                        <div class="col-sm-3 color_t"><b>Caja</b>
                        <input type="text" id="cajass" name="cajass"  class="step__input" disabled>
                        </div>  
                      <div class="col-sm-7 color_t"><b>Observaciones</b>
                      <input type="text" id="obss" name="obss"  class="step__input" disabled>
                      </div>
                  </div>
                  

                </div>

                <div id="vista1">
                    <div>
                        <iframe id="iframePDF" frameborder="0" scrolling="no" width="100%" height="480px"></iframe>
                    </div>
                    
                </div>
              </form>
            </div>
            <div class="modal-footer">
                <a style="float:left;width:220px;height:40px" target="_blank" class="btn btn-flat bg-gradient-primary" id="NuevoPDF">
                      <i class="nav-icon fas fa-file-pdf"></i>&nbsp;&nbsp;Abrir en nueva pestaña </a>
              <button style="float:right;height: 40px;width: 120px;" type="button" class="btn btn-success" id="BotonCerrar1">Cerrar</button>
              
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
  </div>

  <!-- MODAL SEGUIMIENTO-->
  <div class="modal fade" id="modalseguir">
        <div class="modal-dialog modal-xl modal-dialog-centered">
          <div class="modal-content">
          <div class="modal-header" id="modal-header">
              <h4 class="modal-title"  style="font-weight:600" id="modal-title">Seguimiento del Documento: Expediente</h4>&nbsp;&nbsp;
              <p id="nrodesc1" style="color:#8C0505;font-size: 1.4rem;;font-weight:600"></p> 
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
              <div class="modal-body">
                    <table id="tablaSeguimiento" class="table table-hover"   style="width:100%" >
                      <thead style="background: #2874A6;color:white;">
                        <tr style="text-align: center;">
                          <th style="font-weight:600">ID</th>
                          <th>Fecha</th>
                          <th>Ubic. Actual</th>
                          <th>Descripción</th>
                        </tr>
                      </thead>
                      <tbody style="text-align: center;">                           
                      </tbody>        
                          
                      
                      </tbody>
                      <tfoot style="background: #2874A6;color:white;">
                        <tr style="text-align: center;">
                          <th>ID</th>
                          <th>Fecha</th>
                          <th>Ubic. Actual</th>
                          <th>Descripción</th>
                        </tr>
                      </tfoot>
                    </table> 
              </div>
            <div class="modal-footer">
              <button style="height: 40px;width: 120px;" type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
              
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
  </div>  

  <!-- MODAL DERIVACION-->
  <div class="modal fade" id="modalderivar">  
        <div class="modal-dialog modal-lg modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header" id="modal-header">
              <h4 class="modal-title"  style="font-weight:600" id="modal-title">Derivar/Finalizar Documento:</h4>&nbsp;&nbsp;
              <p id="nrodesc" style="color:#8C0505;font-size: 1.4rem;;font-weight:600"></p> 
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="formderivacion">
                <div id="tramite">
                  <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="exp" id="exp">
                            <label>Fecha: </label><span style="color: red;font-weight: 600;"> (*)</span>

                            <div class="input-group date" id="reservationdate" data-target-input="nearest">
                              <input style="width:333px;height:calc(2.25rem + 2px);text-align:center;font-weight:600;font-size:20px" readonly="" type="text" id="datepicker1" value="<?php echo $fechaActual = date('d/m/Y')?>">
                              <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                              </div>
                            </div>

                        </div> 
                      </div>
                        
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Acción: </label><span style="color: red;font-weight: 600;"> (*)</span>
                              <select style="width: 100%;height: 40px;font-weight:600;text-align:center;font-size:18px;" name="idaccion" id="idaccion">                      
                                <option value="1">DERIVAR</option>
                                <option value="2">FINALIZAR</option>
                              </select>
                        </div>     
                      </div>                            
                  </div>
                  <div id="column"class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Área Origen: </label><span style="color: red;font-weight: 600;">(*)</span>
                                <input style="font-weight: 600;text-align:center" type="text" class="form-control" id="idorigen" 
                                readonly="" value="<?php echo $area_actual?>" name="idorigen">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                              <label>Área Destino: </label><span style="color: red;font-weight: 600;"> (*)</span>
                              <select style="width: 100%;height: 40px;font-weight:600;text-align:center;font-size:18px;" name="iddestino" id="iddestino">                      
                              <?php 
                                while($datos=mysqli_fetch_array($area)){
                                ?>
                                    <option value="<?php echo $datos['ID']?>"> <?php echo $datos['area']?></optiomn>
                                <?php 
                                }
                                ?>
                              </select>
                            </div>
                        </div>                           
                  </div>
                  <div  id="des1" class="row"> 
                      <div class="col-sm-12">
                          <div class="form-group">
                            <label>Descripción: </label><span style="color: red;font-weight: 600;">(Opcional)</span>
                            <textarea class="form-control" rows="3" id="iddescripcion" name="iddescripcion" placeholder="Ingrese la descripción..."></textarea>
                          </div>
                        </div>                           
                  </div>
                </div>

              </form>
            </div>
            <div class="modal-footer justify-content-between">
              <button style="height: 40px;width: 120px;" type="button" class="btn btn-danger" data-dismiss="modal" onclick="limpiarderivacion()">Cancelar</button>
              <button style="height: 40px;width: 150px;" type="button" class="btn btn-primary" id="derivar">Aceptar</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
  </div>


  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header">
      <ul class="navbar-nav">
       
        <li class="nav-item">
        <input id="idareaid" name="idarealogin" type="hidden" value="<?php echo $area1['ID'];?>">
        <input id="idarealogin" name="idarealogin" type="hidden" value="<?php echo $area1['area'];?>">
        <input id="idinstitu" name="idinstitu" type="hidden" value="<?php echo $area1['idinstitucion'];?>">
       
        </li>
        <!-- <a class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fas fa-search"><span style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;font-weight: 500;" > Buscar</span></i>
        </a> -->
      </ul>
      
    </nav>
    <!-- /.navbar -->
  
<!-- Main content -->
 <!-- Main content -->
 <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">

              <div class="card card-danger card-outline">
                <div class="card-header ">
                  <h3 class="card-title" style="font-weight:600; color:#084B8A">Documentos Enviados</h3>
                  <!-- <div style="float:right;color:#DE0001;">
                      <label>Listar por: </label>
                        <select style="width:300px;height:30px;font-weight:600;text-align:center;font-size:18px;color:#DE0001;" name="cbovista" id="cbovista">                      
                            <option value="PENDIENTE">PENDIENTE</option>
                            <option value="ACEPTADO">ACEPTADO</option>
                            <option value="RECHAZADO">RECHAZADO</option>
                            <option value="FINALIZADO">FINALIZADO</option>
                        </select>
                  </div> -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  
                <table id="tablaTEnviados" class="tabla_t" style="width:100%" >
                <thead class="tabla_th">
                      <tr >
                        <th rowspan="2">Expediente</th>
                        <th rowspan="2">Nro Doc.</th>
                        <th rowspan="2">Fondo</th>  
                        <th colspan="2">Remitente</th>
                        <th colspan="2">Localización</th>
                        <th rowspan="2">Estado</th>
                        <th  style="width:2px;" rowspan="2">Más...</th>
                        <th rowspan="2">Acción</th>
                      </tr>
                      <tr >
                        <th>CI</th>
                        <th>Datos</th>
                        <th>Origen</th>
                        <th>Actual</th>
                      </tr>
                    </thead>
                    <tbody id="cuerpo" class="tabla_tb">                           
                    </tbody>        
                 
                  </table> 

                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
 
    </div>  
        <!-- /.container-fluid -->
      </section>
      <!-- /.content -->
                            </div>

<script src="/Sistema_MesaPartes/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
</html>

<script>
$( function() {
    $( "#datepicker" ).datepicker(
$.datepicker.regional[ "es" ]
    );
    
} );

</script>
<!-- FIN  DEL CONTENIDO PRINCIPAL -->