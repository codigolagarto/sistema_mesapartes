
<?php 
include("../../config/conexion.php");
include("../../config/conexion2.php");

// Obtener el protocolo (http o https)
$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http";

// Obtener el nombre del host
$host = $_SERVER['HTTP_HOST'];

// Obtener la ruta base
$url_base = "{$protocol}://{$host}/Sistema_MesaPartes/";

if (!isset($_SESSION["idusuarios"])) {
  header("Location: {$url_base}Acceso/");
}

$iduser=$_SESSION["idusuarios"];
$foto=$_SESSION["foto"];
$dni=$_SESSION["dni"];

$consulta=mysqli_query($conexion,"select idinstitucion, area from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$area = mysqli_fetch_assoc($consulta);

$consulta1=mysqli_query($conexion,"select idinstitucion, ae.idarea ID, area from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$area1 = mysqli_fetch_assoc($consulta1);

$institucion=mysqli_query($conexion,"select * from institucion where idinstitucion='1'");
$row = mysqli_fetch_assoc($institucion);

$institucion1=mysqli_query($conexion,"select * from institucion");

$empleados=mysqli_query($conexion,"select p.idpersona ID,  concat('DNI : ',p.dni,' : ',ap_paterno,' ', ap_materno,' ',nombres) Datos
from (usuarios u inner join persona p on u.dni=p.dni) left join empleado e on p.idpersona = e.idpersona
where e.idpersona is null");

$areaE=mysqli_query($conexion,"select i.idareainstitu ID, area from area a, areainstitu i where i.idarea=a.idarea");

?>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<?php
include '../includes/scripts.php'; //nos permite visualizar 
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
<!-- INICIO DEL CONTENIDO PRINCIPAL-->

<?php
	include '../includes/header.php'; //nos permite visualizar 
?>

<!-- Main content modificar -->
<body class="hold-transition sidebar-mini layout-fixed">

<!-- MODAL GESTION DE EMPLEADOS-->
<div class="modal fade" id="modalEmpleado"  >
   <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header btn-primary">
          <h4 style="font-weight:600" class="modal-title">GESTIÓN DE EMPLEADOS:</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">  
          <form id="FormEmpleado">
            <div id="usuar" class="row">
              <div class="col-sm-12">
                <div class="form-group">
                    <label>Usuarios Registrados Pendientes: (*)</label>
                    <select style="font-weight:600;font-size:19px" class="form-control" name="UsuE" id="UsuE">
                      <?php while($datos=mysqli_fetch_array($empleados)) { ?>
                        <option value="<?php echo $datos['ID']  ?>"> <?php echo $datos['Datos'] ?></optiomn>
                      <?php } ?>
                    </select>
                  </div>
              </div>
            </div>
            <div style="border: 1px solid black;padding:10px;" id="Informacion">
              <p style="margin:0;color:#051d49;font-weight:600;text-align:center;font-style: italic; font-weight:600">Información del Usuario:</p>
              <input type="hidden" class="form-control" id="idper" name="idper">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>CI</label>
                      <input style="font-weight:600;font-size:18px" type="text" class="form-control" id="dniU" disabled name="dniU">
                    </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>NOMBRE(S)</label>
                      <input style="font-weight:600;font-size:18px" type="text" class="form-control" id="nomU" disabled name="nomU">
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>APELLIDO PATERNO</label>
                      <input style="font-weight:600;font-size:18px" type="text" class="form-control" id="apU" disabled name="apU">
                    </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>APELLIDO MATERNO</label>
                      <input style="font-weight:600;font-size:18px" type="text" class="form-control" id="amU" disabled name="amU">
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>CELULAR</label>
                      <input style="font-weight:600;font-size:18px" type="text" class="form-control" id="celU" disabled name="celU">
                    </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>DIRECCIÓN</label>
                      <input style="font-weight:600;font-size:18px" type="text" class="form-control" id="dirU" disabled name="dirU">
                    </div>
                </div>
              </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>CÓDIGO (*)</label>
                      <input style="font-weight:600;font-size:18px" type="text" class="form-control" id="codU" name="codU">
                    </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>ÁREA (*)</label>
                        <select style="font-weight:600;font-size:18px" class="form-control" name="areaE" id="areaE">
                          <?php while($datos=mysqli_fetch_array($areaE)) { ?>
                            <option value="<?php echo $datos['ID']  ?>"> <?php echo $datos['area'] ?></optiomn>
                          <?php } ?>
                        </select>
                    </div>
                </div>
              </div>
          </form> 
        </div>
        <div class="modal-footer justify-content-between">
          <button style="height:40px;width:120px" type="button" class="btn btn-danger" data-dismiss="modal" id="SalirE">Cancelar </button>
          <button style="height: 40px;width: 120px;" type="button" class="btn btn-primary" id="EditarE">Editar</button>
              <button style="height: 40px;width: 120px;" type="button" class="btn btn-primary" id="GuardarE">Guardar</button>
        </div>
      </div>
    </div>
</div>

  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header">
      <ul class="navbar-nav">
       
        <li class="nav-item">
        <input id="idareaid" name="idarealogin" type="hidden" value="<?php echo $area1['ID'];?>">
        <input id="idarealogin" name="idarealogin" type="hidden" value="<?php echo $area1['area'];?>">
        <input id="idinstitu" name="idinstitu" type="hidden" value="<?php echo $area1['idinstitucion'];?>">
       
        </li>
        <!-- <a class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fas fa-search"><span style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;font-weight: 500;" > Buscar</span></i>
        </a> -->
      </ul>
      
    </nav>
    <!-- /.navbar -->
  
  </div>
<!-- Main content -->


<section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">

              <div class="card card-danger card-outline">
                <div class="card-header">
                  <h3 class="card-title" style="font-weight:600; color:#084B8A">Listado de Funcionarios Registrados</h3>
                  <a style="float:right;width:220px;height:30px; background-color: #02101f; color:white; border:5px 5px;" class="btn btn-flat" data-toggle="modal" id="NuevoEmpleado">
                    <i class="nav-icon fas fa-plus"></i>&nbsp;&nbsp;Nuevo Registro </a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                <a style="float:right;width:220px;height:30px;" Target="_blank" class="btn btn-flat bg-gray-dark" href="../../reporte/reporte-empleados.php" id="ReportUsu">
                    <i class="nav-iconfas fas fa-file-pdf"></i>&nbsp;&nbsp;Generar Reporte </a>
                <table id="tablaEmpleados" class="tabla_t" style="width:100%" >
                <thead class="tabla_th">
                      <tr>
                        <th>ID</th>
                        <th>Código</th>
                        <th>CI</th>  
                        <th>Apellidos y Nombres</th>
                        <th>Celular</th>
                        <th>Área</th>
                        <th>Acción</th>
                      </tr>
                    </thead>
                    <tbody class="tabla_tb">                           
                    
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
 
    </div>  
        <!-- /.container-fluid -->
      </section>
      <!-- /.content -->

<script src="/Sistema_MesaPartes/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
</html>

<script>
$( function() {
    $( "#datepicker" ).datepicker(
$.datepicker.regional[ "es" ]
    );
    
} );

</script>
<!-- FIN  DEL CONTENIDO PRINCIPAL -->