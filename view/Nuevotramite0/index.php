
<?php 
include("../../config/conexion.php");
include("../../config/conexion2.php");
if (!isset($_SESSION["idusuarios"])) {
    header("Location: /Sistema_MesaPartes/Acceso/");
}

$iduser=$_SESSION["idusuarios"];
$foto=$_SESSION["foto"];
$dni=$_SESSION["dni"];

$consulta=mysqli_query($conexion,"select idinstitucion, ae.idarea IDa, area from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$area = mysqli_fetch_assoc($consulta);

$consulta=mysqli_query($conexion,"select idinstitucion, area from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$area1 = mysqli_fetch_assoc($consulta);
$institucion1=mysqli_query($conexion,"select * from institucion");
$institucion=mysqli_query($conexion,"select * from institucion where idinstitucion='1'");
$row = mysqli_fetch_assoc($institucion);

$consulta=mysqli_query($conexion,"select a.idarea ID,area from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$resultado = mysqli_fetch_assoc($consulta);

$query=mysqli_query($conexion,"SELECT * FROM tipodoc");
$query1=mysqli_query($conexion,"SELECT * FROM tipodoc");

?>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<?php
include '../includes/scripts.php'; //nos permite visualizar 
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
<!-- INICIO DEL CONTENIDO PRINCIPAL-->

<?php
	include '../includes/header.php'; //nos permite visualizar 
?>

<!-- Main content modificar -->
<body class="hold-transition sidebar-mini layout-fixed">

<div class="botones">
  <center>
                <button type="button" class="step__button1" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"><b><i class="bi bi-filetype-doc"></i>Tramite Institucional</b></button>
                <button type="button" class="step__button2 " data-toggle="modal" data-target="#exampleModal2" data-whatever="@fat"><b><i class="bi bi-filetype-doc"></i>Tramite Cooperativa</b></button>
                
  </center>
</div>
  

<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <h5 class="modal-title" id="exampleModalLabel"><i class="bi bi-filetype-doc"></i><b>NUEVO DOCUMENTO - TRAMITE INSTITUCIONAL</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="root">
        <form id="formulario-tramite" onsubmit="submitForm(event)" name="formulario-tramite" enctype="multipart/form-data" method="post" class="form-register">
            <div class="form-register__header">
                <ul class="progressbar">
                    <li class="progressbar__option active">DATOS DEL REMITENTE <i class="bi bi-clipboard2"></i></li>
                    <li class="progressbar__option">INVENTARIO GENERAL </li>
                    <li class="progressbar__option">DATOS DEL DOCUMENTO </li>
                    
                </ul>
               
            </div>
            <div class="form-register__body">
                <div class="step active" id="step-1">
                    <div class="step__header">
                        <h2 class="step__title">DATOS DEL REMITENTE <i class="bi bi-clipboard2"></i></h2>
                    </div>
                    <div class="step__body">
                      <div id="contenedor">
                      <input type="hidden" class="form-control" id="idpersona" name="idpersona">
                      
                          <div class="wd20">
                          <input type="text" onkeypress='return validaNumericos(event)' maxlength="8"    minlength="8" name="iddni" id="iddni" required placeholder="Carnet de Identidad" class="step__input">
                          </div>
                          <div class="wd20">
                          <input id="validar" type="button" class="step__boton" value="Validar">
                          </div>
                          <div class="wd20"></div> <div class="wd20"></div>
                      </div>
                     
                  <!---
                    <input type="text" onkeypress='return validaNumericos(event)' maxlength="8"    minlength="8" name="iddni" id="iddni" required placeholder="Carnet de Identidad" class="step__input">
                        <input type="button" class="step__boton" value="Validar">--->
                        <div id="contenedor">
                           
                            <input type="text" id="idnombre" name="idnombre" placeholder="Nombre(s)" required class="step__input">  
                           
                        </div>
                        <div id="contenedor">
                            
                            <input type="text" id="idam" name="idam" placeholder="Apellido Paterno" required class="step__input">
                            
                        </div>
                        <div id="contenedor">
                            
                            <input type="text" id="idap" name="idap" placeholder="Apellido Materno" required class="step__input">
                            
                        </div>
                        <div id="contenedor">
                            <input type="text" id="idcel" onkeypress='return validaNumericos(event)' minlength="9" name="idcel" placeholder="Celular" required class="step__input">
                         
                            
                        </div>
                    
                        
                        <input type="text" id="iddirec" name="iddirec" placeholder="Dirección" required class="step__input">
                        <input type="text" id="idcorre" name="idcorre" placeholder="Correo Electrónico" required class="step__input">
                       
                    </div>
                    <div class="step__footer">
                        <button type="button" class="step__button step__button--next" data-to_step="2" data-step="1">Siguiente</button>
                    </div>
                </div>
                <div class="step" id="step-2">
                    <div class="step__header">
                        <h2 class="step__title"><i class="bi bi-clipboard2-data"></i>INVENTARIO GENERAL</h2>
                    </div>
                    <div class="step__body">
             <div id="contenedor">
                    <div class="wd30">
                    <select class="step__input" name="idtipo" id="idtipo">
                          <?php  while($datos=mysqli_fetch_array($query))  {?>
                             <option value="<?php echo $datos['idtipodoc']  ?>"> <?php echo $datos['tipodoc'] ?></optiomn>
                          <?php } ?>
                    </select>
                    </div>
                    <div class="wd30">
                                  <input type="text" id="idnrodoc" onkeypress='return validaNumericos(event)' required name="idnrodoc" placeholder="N° Documento"  class="step__input">
                                  </div>
                                  <div class="wd30">
                                  <input type="number" id="idfolios" required name="idfolios" placeholder="N° Folios" class="step__input">
                                </div>
      </div>
                              
                                <div id="contenedor"> 
                                  <div class="wd30"><input type="text" id="idasunto" required name="idasunto" placeholder="Asunto" class="step__input"> </div>
                                  <div class="wd30">  <input type="text" id="idfondo" required name="idfondo" placeholder="Fondo" class="step__input"></div>
                                  <div class="wd30"><input type="text" id="idseccion" required name="idseccion" placeholder="Sub Sección" class="step__input"></div>
                                </div>
                                <div id="contenedor"> 
                                
                                  <div class="wd30"><input type="number" id="idsubsec" required name="idsubsec"  placeholder="Sub Sección" class="step__input"></div>
                                  <div class="wd30"> <input type="text" id="idserie"  required name="idserie" placeholder="Serie" class="step__input"></div>
                                  <div class="wd30"> <input type="number" id="idsubserie" required name="idsubserie"  placeholder="Sub Serie" class="step__input"></div> 
                                </div>
                                
            
                                <input type="text" id="idcodref" required name="idcodref" placeholder="Código de Referencia" class="step__input">
                                <label style="text-align:center;">Fechas Extremas <i class="bi bi-calendar4-week"></i></label><br>
                                <div id="contenedor">
                                  <div class="wd50">
                                  <span>Fecha de Inicio</span>
                                    <input type="date" id="idfechaini" required name="idfechaini"  placeholder="Fecha de Inicio" class="step__input">
                                  </div>
                                  <div class="wd50">
                                  <span>Fecha de Finalización</span>
                                    <input type="date" id="idfechafin" required name="idfechafin" placeholder="Fecha de Finalización" class="step__input">
                                  </div>
                                </div>
                                
                                <div id="contenedor">
                                  <div class="wd50"> <input type="number" id="idunidoc" required name="idunidoc"  placeholder="Cantidad de Unidades Documentales" class="step__input"></div>
                                  <div class="wd50"><input type="number" id="idmeli" required name="idmeli" placeholder="Extensión en Metros Líneales" class="step__input"></div>
                                </div>
                              
                        <!--<textarea rows="4" cols="80" placeholder="Dirección" class="step__input"></textarea>-->
                    </div>
                    <div class="step__footer">
                        <button type="button" class="step__button step__button--back" data-to_step="1" data-step="2">Regresar</button>
                        <button type="button" class="step__button step__button--next" data-to_step="3" data-step="2">Siguiente</button>
                    </div>
                </div>
                <div class="step" id="step-3">
                    <div class="step__header">
                        <h2 class="step__title"> <i class="bi bi-clipboard"></i> DATOS DEL DOCUMENTO</h2>
                    </div>
                    <div class="step__body">
                    
                      <div id="contenedor">
                        <div class="wd30">
                        <label>Área de Identificación</label>
                        </div>
                        <div class="wd10"><input type="number" id="idcodrefe" required name="idcodrefe" placeholder="Código de Referencia" class="step__input" min=0>
                        </div><div class="wd10"><input type="number" id="idnrocaja" required name="idnrocaja" placeholder="N° Caja" class="step__input" min=0>
                        </div>
                        <div class="wd30">
                        <input type="text" id="idtitulo" required name="idtitulo" placeholder="Título de Documento" class="step__input">
                        </div>
                      </div>
                      <div id="contenedor">
                        <div class="wd30">
                        <label style="text-align:center;">Fechas Extremas <i class="bi bi-calendar4-week"></i></label><br>
                        </div>
                        <div class="wd30">
                        <span>Fecha de Inicio</span>
                          <input type="date" id="idfechainicio" required name="idfechainicio"  placeholder="Fecha de Inicio" class="step__input">
                          </div><div class="wd30">
                          <span>Fecha de Finalización</span>
                          <input type="date" id="idfechafinal" required name="idfechafinal" placeholder="Fecha de Finalizacion" class="step__input">
                          </div>
                      </div>

                      <div id="contenedor">
                        <div class="wd30">
                        <input type="number" id="idtomo" required  name="idtomo" placeholder="N° Tomo" class="step__input">
                        </div><div class="wd30">
                        <input type="number" id="idfojas" required name="idfojas" placeholder="Fojas" class="step__input">
                        </div><div class="wd30">
                        <input type="number"  id="idsoporte" required name="idsoporte" placeholder="Soporte Técnico" class="step__input">
                        </div>
                      </div>
                      <input type="text" id="idunidad" required name="idunidad" placeholder="Nombre de la Unidad Productora de los Documentos" class="step__input">
                      
                      <div id="contenedor">
                        <div class="wd20">
                        <label>Ubicación Topografica</label>
                        </div>
                        <div class="wd20">
                        <input type="number" id="idestante" required name="idestante" placeholder="Estante" class="step__input"> 
                        </div><div class="wd20">
                        <input type="number" id="idcuerpo" required    name="idcuerpo" placeholder="Cuerpo" class="step__input">
                        </div><div class="wd20">
                        <input type="number" id="idvalda" required name="idvalda" placeholder="Balda" class="step__input">
                        </div>
                      </div>
                    <div id="contenedor">
                                    <div class="wd50">  <textarea rows="2" cols="80"  id="idobs" required name="idobs" placeholder="Observaciones" class="step__input"></textarea></div>
                                    <div class="wd50"><label>Adjuntar archivo (pdf.)</label>
                                    <div class="enviar">
                                    <p id="alias"></p>
                                    <label for="idfile" id="archivo">Elige el Archivo...</label>
                                    <input type="file" id="idfile" name="idfile" required accept="application/pdf">
                      </div>
</div>
                    </div>

                      <div class="custom-control custom-checkbox">
                                <input class="form-check-input" type="checkbox"
                                    id="check" name="check" value="option1" required>

                                <label for="customCheckbox4" class="form-check-label">&nbsp;Declaro que la
                                    información proporcionada es válida y verídica.
                                  <span style="color: red;">(*)</span></label>    
                      </div>
                            <br>
                    </div>
                    <div class="step__footer">
                        <button type="button" class="step__button step__button--back" data-to_step="2" data-step="3">Regresar</button>
                        <button type="submit" id="Enviar" class="step__button step__button--next" onclick="return RegistroDocumento()">Enviar Trámite</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
      </div>
    
    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-lg" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <h5 class="modal-title" id="exampleModalLabel"><i class="bi bi-filetype-doc"></i><b>NUEVO DOCUMENTO - TRAMITE COOPERATIVA</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
  dos
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div>
    </div>
  </div>
</div>
        
<script>
      let archivo = document.querySelector('#idfile');
    archivo.addEventListener('change',() => {
        document.querySelector('#alias').innerText=archivo.files[0].name;
    });
  </script>
</body>
<script src="/Sistema_MesaPartes/public/assets/css/prueba.js"></script>


      <strong>Copyright &copy; 2024 <a href=" /Sistema_MesaPartes/"> <?php echo $row['razon']?></a>.</strong>
      Todos los derechos reservados.
      <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0
      </div>
    </footer>
    <aside class="control-sidebar control-sidebar-dark">
    </aside>
  </div>
 
  <script>
$( function() {
    $( "#datepicker" ).datepicker(
$.datepicker.regional[ "es" ]
    );
    
} );

let archivo = document.querySelector('#idfile');
    archivo.addEventListener('change',() => {
        document.querySelector('#alias').innerText=archivo.files[0].name;
    });
</script>
  

<!-- FIN  DEL CONTENIDO PRINCIPAL -->