<?php 
 include("../../config/conexion.php");
 include("../../config/conexion2.php");
if (!isset($_SESSION["idusuarios"])) {
  header("Location: /Sistema_MesaPartes/Acceso/");
}
$iduser=$_SESSION["idusuarios"];
$foto=$_SESSION["foto"];
$dni=$_SESSION["dni"];

$consulta=mysqli_query($conexion,"select idinstitucion, area from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$area = mysqli_fetch_assoc($consulta);

$consulta1=mysqli_query($conexion,"select idinstitucion, ae.idarea ID, area from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$area1 = mysqli_fetch_assoc($consulta1);

$institucion=mysqli_query($conexion,"select * from institucion where idinstitucion='1'");
$row = mysqli_fetch_assoc($institucion);

$institucion1=mysqli_query($conexion,"select * from institucion");

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php
include '../includes/scripts.php'; //nos permite visualizar 
?>
</head>

<body class="hold-transition sidebar-mini layout-fixed">

<?php
	include '../includes/header.php'; //nos permite visualizar 
?>
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header"><ul class="navbar-nav"><li class="nav-item">
        <input id="idareaid" name="idarealogin" type="hidden" value="<?php echo $area1['ID'];?>">
        <input id="idarealogin" name="idarealogin" type="hidden" value="<?php echo $area1['area'];?>">
        <input id="idinstitu" name="idinstitu" type="hidden" value="<?php echo $area1['idinstitucion'];?>"></li>
        </ul>
    </nav>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">

              <div class="card card-primary" id="insert">
                <div class="card-header">
                  <h3 class="card-title" style="font-weight:600; color:white"><i
                      class="fas fa-search"></i>&nbsp;&nbsp;BÚSQUEDA DE Documentos</h3>
                </div>

                <!-- /.card-header -->
                <div class="card-body">
                  <h3 style="font-size: 18px;">Para realizar la búsqueda de un documento presentado debe de ingresar el
                    Número de <b>Expediente del Documento</b> y seleccionar el año de presentación:</h3>
                  <br>
                  <div>

                    <form id="FormBuscar">
                      <div class="form-group row">
                        &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label class="etiqueta">Nro Expediente:</label>
                        <div class="col-sm-2">
                          <input type="email" class="form-control" id="idexpb"
                            onkeypress="return validaNumericos(event)" maxlength="6">
                        </div>
                        &nbsp;&nbsp;&nbsp;
                        <label class="etiqueta">CI:</label>
                        <div class="col-sm-2">
                          <input type="email" class="form-control" id="iddnii"
                            onkeypress="return validaNumericos(event)" maxlength="8">
                        </div>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label class="etiqueta">Año:</label>
                        <div class="col-sm-2">
                          <select style="width:150px; font-weight:700; font-size:20px" class="form-control"
                            id="idtipob">
                            <option value="2024">2024</option>
                          </select>

                        </div>

                        <div class="col-sm-3">
                          <button style="width:200px;height:40px;font-size:18px;float: right;" type="button"
                            id="btnBusca" class="btn btn-danger btn-block"><i
                              class="fa fa-search"></i>&nbsp;&nbsp;&nbsp;BUSCAR</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>

              </div>

              <div id="divNoFound">
                <div class="callout callout-warning">
                  <div class="row">
                    <div class="col-sm-3" align="right">
                      <img style="width: 140px; height: 140px;"
                        src="/Sistema_MesaPartes/public/assets/img/error-404.png">
                    </div>
                    <div class="col-sm-9">
                      <br>
                      <h3><i class="fas fa-exclamation-triangle text-warning"></i> DOCUMENTO NO ENCONTRADO.</h3>

                      <p style="font-size:18px;">
                        No se encontro el trámite con los datos ingresado, puede ser por que no existe un documento
                        registrado con esos datos.<br>
                        <b>Por favor, intente realizar la búsqueda ingresando los datos correctos.<b>
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              <div class="card card-olive" id="dat">
                <div class="card-header">
                  <h3 class="card-title"><i class="fas fa-file-pdf "></i>&nbsp;&nbsp;
                    DATOS DEL DOCUMENTO REALIZADO
                  </h3>
                </div>
                <div class="row">
                  <div class="col-sm-7">

                  </div>
                  <div class="col-sm-5">
                    <br>
                    <div class="row">
                      <div class="col-md-6">

                        <button type="button" style="height:40px" class="btn btn-primary btn-block" id="btnNew"><i
                            class="fa fa-search"></i>&nbsp;&nbsp;Nueva Búsqueda</button>
                      </div>
                      <div class="col-md-5">
                        <button type="button" style="height:40px" class="btn btn-danger btn-block" id="btnhistorial"><i
                            class="fa fa-plus"></i>&nbsp;Mostrar Historial</button>
                      </div>
                      <div class="col-md-1">

                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="callout callout-success">

                        <table width="100%" border="2" cellspacing="0" cellpadding="5" id="tableDoc">
                          <tr>
                            <th colspan="2" style="background:#896A09;text-align:center;color:white">
                              <h5 style="font-weight:600">DATOS DEL DOCUMENTO</h5>
                              </font>
                            </th>
                          </tr>
                          <tr style="text-align:center;font-size:20px">
                            <th style="background:#F2E3A9;">Expediente</th>
                            <td>
                              <p id="celdaexpe"></p>
                            </td>
                          </tr>
                          <tr style="text-align:center;font-size:20px">
                            <th style="background:#F2E3A9;">N° Documento</th>
                            <td>
                              <p id="celdanro"></p>
                            </td>
                          </tr>
                          <tr style="text-align:center;font-size:20px">
                            <th style="background:#F2E3A9;">Tipo</th>
                            <td>
                              <p id="celdatipo"></p>
                            </td>
                          </tr>
                          <tr style="text-align:center;font-size:18px">
                            <th style="background:#F2E3A9">Asunto</th>
                            <td>
                              <p id="celdasunto"></p>
                            </td>
                          </tr>
                        </table>

                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="callout callout-info">

                        <table width="100%" border="2 black " cellspacing="0" cellpadding="5" id="tableRemitente">
                          <tr>
                            <th colspan="2" style="background:#BDBDBD ; text-align:center">
                              <h5 style="font-weight:600">DATOS DEL REMITENTE</h5>
                              </font>
                            </th>
                          </tr>
                          <tr style="text-align:center;font-size:18px">
                            <th style="background:#D9D9D8;">CI</th>
                            <td>
                              <p id="celdadni"></p>
                            </td>
                          </tr>
                          <tr style="text-align:center;font-size:18px">
                            <th style="background:#D9D9D8;r">Apellidos y Nombres</th>
                            <td>
                              <p id="celdadatos"></p>
                            </td>
                          </tr>
                          
                              <p id="celdaenti"></p>
                            </td>
                          </tr>
                        </table>

                      </div>
                    </div>
                  </div>

                </div>

              </div>

              <!-- LINEA DE TIEMPO DEL DOCUMENTO -->
              <div id="linea">
 
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>

    </div>
    <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

</div>

  <footer class="main-footer">

    <strong>Copyright &copy; 2024 <a href=" /Sistema_MesaPartes/">
        <?php echo $row['razon']?>
      </a>.</strong>
    Todos los derechos reservados.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0
    </div>
  </footer>
  <aside class="control-sidebar control-sidebar-dark">
  </aside>
  </div>



</body>

</html> 