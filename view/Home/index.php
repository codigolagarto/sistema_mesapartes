
<?php 
include("../../config/conexion.php");
include("../../config/conexion2.php");

// Obtener el protocolo (http o https)
$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http";

// Obtener el nombre del host
$host = $_SERVER['HTTP_HOST'];

// Obtener la ruta base
$url_base = "{$protocol}://{$host}/Sistema_MesaPartes/";

if (!isset($_SESSION["idusuarios"])) {
  header("Location: {$url_base}Acceso/");
}
$iduser=$_SESSION["idusuarios"];
$foto=$_SESSION["foto"];
$dni=$_SESSION["dni"];

$consulta=mysqli_query($conexion,"select idinstitucion, ae.idarea IDa, area from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$area = mysqli_fetch_assoc($consulta);


$consulta1=mysqli_query($conexion,"select idinstitucion, ae.idarea ID, area from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$area1 = mysqli_fetch_assoc($consulta1);

$institucion=mysqli_query($conexion,"select * from institucion where idinstitucion='1'");
$row = mysqli_fetch_assoc($institucion);

$consulta=mysqli_query($conexion,"select a.idarea Ida from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$res = mysqli_fetch_assoc($consulta);
$ida = $res['Ida'];

$cant=mysqli_query($conexion,"SELECT count(*) total FROM documento where estado='PENDIENTE'");
$fila = mysqli_fetch_assoc($cant);
$cant1=mysqli_query($conexion,"SELECT count(*) total FROM documento where estado='RECHAZADO'");
$fila1 = mysqli_fetch_assoc($cant1);
$cant2=mysqli_query($conexion,"SELECT count(*) total FROM documento where estado='ACEPTADO'");
$fila2 = mysqli_fetch_assoc($cant2);

$cant=mysqli_query($conexion,"SELECT count(*) total FROM documento where estado='PENDIENTE' and idubi='$ida'");
$filaP = mysqli_fetch_assoc($cant);
$cant=mysqli_query($conexion,"SELECT count(*) total FROM documento where estado='RECHAZADO' and idubi='$ida'");
$filaR = mysqli_fetch_assoc($cant);
$cant=mysqli_query($conexion,"SELECT count(*) total FROM documento where estado='ACEPTADO' and idubi='$ida'");
$filaA = mysqli_fetch_assoc($cant);

$consulta=mysqli_query($conexion,"SELECT distinct date_format(fechad,'%Y') año FROM derivacion");


?>
 
   


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php
include '../includes/scripts.php'; //nos permite visualizar 
?>
</head>
<body>
  <!-- INICIO DEL CONTENIDO PRINCIPAL-->

  <?php
	include '../includes/header.php'; //nos permite visualizar 
?>

  <section class="content">
    <div class="container-fluid">

      <div class="row">

        <section class="col-lg-12 connectedSortable">


        <?php if($area['area'] == "ADMIN SISTEMA" or $area['area'] == "ARCHIVO CENTRAL"){?>
          
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 style="font-size: 1.2rem;font-weight: 500;">
                <i class="ion ion-md-folder-open mr-1"></i>&nbsp;<b>INFORMACIÓN DOCUMENTARIA GENERAL:<b></h3>
              <div class="row">
                <div class="col-md-4 col-sm-6 col-12">
                  <div class="info-box bg-danger">
                    <span class="info-box-icon bg-danger"><img src="/Sistema_MesaPartes/public/assets/img/rechazado.png"></span>
                    <div class="info-box-content">
                      <span style="font-weight:600;font-size:20px;" class="info-box-text">RECHAZADOS</span>
                      <span style="font-weight:600;font-size:40px;" ><?php echo $fila1['total']?></span>
                      <span style="font-weight:500;font-size:18px;" class="progress-description">Total de Documentos</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                  <div class="info-box bg-primary">
                    <span class="info-box-icon bg-primary"><img src="/Sistema_MesaPartes/public/assets/img/pendiente.png"></span>
                    <div class="info-box-content">
                      <span style="font-weight:600;font-size:20px;" class="info-box-text">PENDIENTES</span>
                      <span style="font-weight:600;font-size:40px;"><?php echo $fila['total']?></span>
                      <span style="font-weight:500;font-size:18px;" class="progress-description">Total de Documentos</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                  <div class="info-box bg-green">
                    <span class="info-box-icon bg-green"><img src="/Sistema_MesaPartes/public/assets/img/documentos.png"></span>
                    <div class="info-box-content">
                      <span style="font-weight:600;font-size:20px;" class="info-box-text">ACEPTADOS</span>
                      <span style="font-weight:600;font-size:40px;" ><?php echo $fila2['total']?></span>
                      <span style="font-weight:500;font-size:18px;" class="progress-description">Total de Documentos</span>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- /.card-header -->
          </div>
        <?php }?>

        <div class="card card-outline card-fuchsia">
            <div class="card-header">
              <h3 style="font-size: 1.2rem;font-weight: 500;">
                <i class="ion ion-md-folder-open mr-1"></i>&nbsp;<b>RESUMEN DE TRÁMITES DEL ÁREA:<b></h3>
              <div class="row">
                <div class="col-md-4 col-sm-6 col-12">
                  <div class="info-box bg-danger">
                    <span class="info-box-icon bg-danger"><img src="/Sistema_MesaPartes/public/assets/img/rechazado.png"></span>
                    <div class="info-box-content">
                      <span style="font-weight:600;font-size:20px;" class="info-box-text">RECHAZADOS</span>
                      <span style="font-weight:600;font-size:40px;" ><b id="cantR"><?php echo $filaR['total'];?></b></span>
                      <span style="font-weight:500;font-size:18px;" class="progress-description">Total de Documentos</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                  <div class="info-box bg-primary">
                    <span class="info-box-icon bg-primary"><img src="/Sistema_MesaPartes/public/assets/img/pendiente.png"></span>
                    <div class="info-box-content">
                      <span style="font-weight:600;font-size:20px;" class="info-box-text">PENDIENTES</span>
                      <span style="font-weight:600;font-size:40px;"><b id="cantP"><?php echo $filaP['total'];?></b></span>
                      <span style="font-weight:500;font-size:18px;" class="progress-description">Total de Documentos</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                  <div class="info-box bg-green">
                    <span class="info-box-icon bg-green"><img src="/Sistema_MesaPartes/public/assets/img/documentos.png"></span>
                    <div class="info-box-content">
                      <span style="font-weight:600;font-size:20px;" class="info-box-text">ACEPTADOS</span>
                      <span style="font-weight:600;font-size:40px;" ><b id="cantA"><?php echo $filaA['total'];?></b></span>
                      <span style="font-weight:500;font-size:18px;" class="progress-description">Total de Documentos</span>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- /.card-header -->
        </div>
       
        </section>

      </div>

    </div>
  </section>

</div>
<!-- FIN  DEL CONTENIDO PRINCIPAL -->
