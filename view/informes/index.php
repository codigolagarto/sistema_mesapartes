<?php

include("../../config/conexion.php");
include("../../config/conexion2.php");

// Obtener el protocolo (http o https)
$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http";

// Obtener el nombre del host
$host = $_SERVER['HTTP_HOST'];

// Obtener la ruta base
$url_base = "{$protocol}://{$host}/Sistema_MesaPartes/";

if (!isset($_SESSION["idusuarios"])) {
  header("Location: {$url_base}Acceso/");
}
$iduser=$_SESSION["idusuarios"];
$foto=$_SESSION["foto"];
$dni=$_SESSION["dni"];
$consulta=mysqli_query($conexion,"select idinstitucion, ae.idarea IDa, area from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$area = mysqli_fetch_assoc($consulta);


$consulta1=mysqli_query($conexion,"select idinstitucion, ae.idarea ID, area from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$area1 = mysqli_fetch_assoc($consulta1);

$institucion=mysqli_query($conexion,"select * from institucion where idinstitucion='1'");
$row = mysqli_fetch_assoc($institucion);

$consulta=mysqli_query($conexion,"select a.idarea Ida from empleado e, persona p, areainstitu a, area ae
where e.idpersona=p.idpersona and e.idareainstitu=a.idareainstitu and ae.idarea=a.idarea and dni='$dni';");
$res = mysqli_fetch_assoc($consulta);
$ida = $res['Ida'];



$consulta=mysqli_query($conexion,"SELECT distinct date_format(fechad,'%Y') año FROM derivacion");
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php
include '../includes/scripts.php'; //nos permite visualizar 
?>
</head>

<body class="hold-transition sidebar-mini layout-fixed">

<?php
	include '../includes/header.php'; //nos permite visualizar 
?>



          <div class="row">
            <div class="col-sm-6">
              <div align="center">
                  <label>SECCION</label>
                  <select  class="form-control" style="width:300px;height:45px;font-weight:600;text-align:center;font-size:18px" name="cbovista" id="cboreport">                      
                  <?php $consulta_d=mysqli_query($conexion, "select *  from tipodoc");
                  $query=mysqli_num_rows($consulta_d);
                  if ($query > 0) {
                      while ($data = mysqli_fetch_array($consulta_d)) {?> 
                      <option value="<?php echo $data[0];?>"><?php echo $data[1];?></option><?php }} ?> 
                  </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div align="center">
                    <label>Forma del Reporte: </label>
                    <select  class="form-control"  style="width:300px;height:45px;font-weight:600;text-align:center;font-size:18px;" name="cboreport1" id="cboreport1">                      
                        <option value=0>Seleccione</option> 
                        <option value=3>POR RANGO DE FECHAS</option>
                    </select>
                </div>
            </div>
          </div>
            <br>

            <form id="formreport">
          <div class="row" id="re">
            <div class="col-sm-6">
              <div align="center" id="reportaño">
                  <label>AÑO: </label>
                  <select class="form-control" style="width:300px;height:45px;font-weight:600;text-align:center;font-size:18px" name="cboaño" id="cboaño">                      
                                <?php while($datos=mysqli_fetch_array($consulta)) {?>
                                    <option value="<?php echo $datos['año']?>"> <?php echo $datos['año']?></option>
                                <?php }?>
                  </select>
              </div>
            </div>
            
          </div>
          <br>
            <div class="row" id="reportrango">
                <div class="col-sm-12">
                  <div id="sandbox-container">
                    <div class="input-daterange input-group" id="datepicker">
                        <span class="input-group-addon">DESDE: &nbsp;</span>
                        <input type="date" class="input-sm form-control" name="start" id="start" value=""/>
                        <span class="input-group-addon">&nbsp; HASTA: &nbsp;</span>
                        <input type="date" class="input-sm form-control" name="end" id="end" value=""/>
                    </div>
                  </div>
                </div>
            </div>

          </form>

        </div>
        <div class="modal-footer justify-content-between">
          <button style="height:40px;width:120px" type="button" class="btn btn-danger" data-dismiss="modal">Cerrar </button>
          <button style="height:40px;width:180px" type="button" class="btn btn-primary" id="btnReport">Generar Reporte</button>
        </div>
      </div>
    </div>
</div>



<script src="/Sistema_MesaPartes/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>