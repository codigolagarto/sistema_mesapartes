<?php
//session_start();

?>
<head>
<title>Sistema de Gestión Documental</title>

<?php
	include '../includes/nav.php'; //nos permite visualizar 
?>
</head>

<!-- MODAL FOTO-->
<div class="modal fade" id="modalfotop"  >
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header btn-primary" >
        <h4 style="font-weight:400" class="modal-title">ACTUALIZAR FOTO DE PERFIL:</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"><i class="bi bi-x-circle-fill btn-primary-border-subtle"></i></span>
        </button>
        </div>
        <form id="FormFotop">
        <div style="text-align:center;" class="modal-body">
        <h1 style="font-family:arial;font-size:20px;font-weight:600">Foto de perfil Actual</h1>  
            <img style="widht: 150px; height:150px;" src="/Sistema_MesaPartes/<?php echo $foto?>" id="Fotope" name="Fotope">
        <br><br>
        <div class="form-group">
            <label>Elegir Foto (jpg)</label><span style="color: red;font-weight: 600;"> (*)</span>
            <div class="file">
                <input type="hidden" id="opcion" name="opcion" value='13'>
                <input type="hidden" id="iddni2" name="iddni2" value="<?php echo $dni;?>">
                <input type="hidden" id="idusua2" name="idusua2" value="<?php echo $iduser;?>">
                <input type="file" id="idfilep" name="idfilep" required accept=".jpg">
            </div>
        </div>
        </div>
        <div class="modal-footer justify-content-between">
        <button style="height:40px;width:120px" type="button" class="btn btn-danger" data-dismiss="modal">Cancelar </button>
        <button style="height:40px;width:120px" type="submit" class="btn btn-primary" id="CambiarP">Cambiar</button>
        </div>
        </form>
    </div>
    </div>
</div>

<!-- MODAL CONFIRMACION CERRAR SESION -->
<div class="modal fade" id="mimodal" aria-modal="true" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header btn-primary">
        <h4 class="modal-title">Confirmación:</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        </div>
        <div class="modal-body">
        <p>¿Seguro que quiere cerrar la Sesión Actual?</p>
        </div>
        <div class="modal-footer justify-content-between">
        <button style="height:40px;width:120px" type="button" class="btn btn-danger" data-dismiss="modal">No. Continuar </button>
        <button style="height:40px;width:120px" type="button" class="btn btn-primary" onclick="salir()">Sí. Salir</button>
        </div>
    </div>
    </div>
</div>


