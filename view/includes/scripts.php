<style>
.modal-header-primary {
    color:#fff;
    padding:9px 15px;
    border-bottom:1px solid #eee;
    background-color: #055FB5;
    -webkit-border-top-left-radius: 4px;
    -webkit-border-top-right-radius: 4px;
    -moz-border-radius-topleft: 4px;
    -moz-border-radius-topright: 4px;
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
}
#Enviar {

}
.file input {
display: none;
}
.file label {
font-weight: 500;
display: block;
padding: 8px 21px;
border: 1px solid #ccc;
border-radius: 6px;
background-color: #1184ff;
color: white;
cursor: pointer;
text-align: center;
font-size: 16px;
}
#validar {
align-items: center;
}
</style>

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<link rel="stylesheet" href="/Sistema_MesaPartes/public/assets/plugins/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="/Sistema_MesaPartes/public/assets/dist/css/adminlte.min.css">
<link rel="icon shortcut" href="/Sistema_MesaPartes/public/assets/img/logo.png">
<link rel="stylesheet" href="/Sistema_MesaPartes/public/assets/fonts/ionicons.css">
<link rel="stylesheet" href="/Sistema_MesaPartes/public/assets/fonts/feather.css">
<link rel="stylesheet"  href="/Sistema_MesaPartes/public/assets/plugins/datepicker/css/bootstrap-datepicker3.min.css">

<link rel="stylesheet" href="/Sistema_MesaPartes/public/assets/css/prueba.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 

<link rel="stylesheet"  href="/Sistema_MesaPartes/public/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="/Sistema_MesaPartes/public/assets/plugins/select2/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="/Sistema_MesaPartes/public/assets/plugins/jquery-ui-1.13.1.custom/jquery-ui.css">

<script src="/Sistema_MesaPartes/public/assets/plugins/jquery/jquery.min.js"></script>
<script src="/Sistema_MesaPartes/public/assets/plugins/bootstrap/js/bootstrap.js"></script>
<script src="/Sistema_MesaPartes/public/assets/dist/js/adminlte.js"></script>
<script src="/Sistema_MesaPartes/public/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script> 

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="/Sistema_MesaPartes/public/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/Sistema_MesaPartes/public/assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="/Sistema_MesaPartes/public/assets/plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/Sistema_MesaPartes/public/assets/plugins/datepicker/locales/bootstrap-datepicker.es.min.js"></script>
<script src="//code.jquery.com/jquery-1.12.4.js"></script>
<!-- DataTables  & Plugins -->
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/Sistema_MesaPartes/public/assets/js/main.js"></script> 
<script src="/Sistema_MesaPartes/public/assets/plugins/jquery-ui-1.13.1.custom/jquery-ui.js"></script>
<script src="/Sistema_MesaPartes/public/assets/plugins/jquery-ui-1.13.1.custom/datepicker-es.js"></script>


<script>
//$('#sandbox-container .input-daterange').datepicker({
//format: "dd/mm/yyyy",
//endDate: "<?php echo date("d/m/Y");?>",
//autoclose: true
//});
</script>