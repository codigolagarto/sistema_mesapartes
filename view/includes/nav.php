<div class="modal fade" id="modalUsu">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header btn-primary" id="modal-header">
            <h4 style="font-weight:600" class="modal-title" id="modal-title">DATOS DEL  PERFIL DEL USUARIO</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
            </button>
            </div>
            <div class="modal-body">
                <form id="formperfil">
                    <input type="hidden" class="form-control" name="idusup" id="idusup">
                    <input type="hidden" class="form-control" name="idperp" id="idperp">
                    <div class="row">
                        <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputName">CI</label>
                            <input type="text" class="form-control" name="idnip" id="idnip" onkeypress='return validaNumericos(event)' maxlength="8" minlength="8">
                        </div>
                        </div>
                        <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputName">Nombres</label>
                            <input type="text" class="form-control" name="inombrep" id="inombrep">
                        </div>
                        </div>
                    </div>
                <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="inputName">Apellido Paterno</label>
                                <input type="text" class="form-control" name="iappatp" id="iappatp">
                            </div>
                        </div>
                        
                        <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputName">Apellido Materno</label>
                            <input type="text" class="form-control" name="iapmatp" id="iapmatp">
                        </div>
                        </div>
                      </div>                                        

                      <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                              <label for="inputEmail">Celular</label>
                              <input type="text" class="form-control"  name="icelp" id="icelp">
                          </div>
                        </div>
                        
                        <div class="col-sm-6">
                          <div class="form-group">
                              <label for="inputEmail">Dirección</label>
                              <input type="text" class="form-control"  name="idirp" id="idirp">
                          </div>
                        </div>                      
                      </div>
                      
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                              <label for="inputMessage">Email</label>
                              <input type="email" class="form-control1"  name="iemailp" id="iemailp">
                          </div>
                        </div>
                        
                        <div class="col-sm-6">
                          <div class="form-group">
                              <label for="inputEmail">Nombre Usuario</label>
                              <input type="text" class="form-control1"  name="inomusup"  id="inomusup">
                          </div>
                        </div>
                      </div>
                  
                </form>
            </div>
            <div class="modal-footer justify-content-between">
              <button style="height: 40px;width: 120px;" type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
              <button style="height: 40px;width: 180px;" type="button" class="btn btn-primary" id="Actualizar">Actualizar Datos</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
  </div>


<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand prueba_col">
    <ul class="navbar-nav">
        <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>&nbsp;&nbsp;&nbsp;
        <li class="nav-item">
        <h3 style="margin:8px 0;font-size:20px;font-weight:600">ÁREA: <?php echo $area1['area'];?></h3>
        <input id="idarealogin" name="idarealogin" type="hidden" value="<?php echo $area1['area'];?>">
        <input id="idinstitu" name="idinstitu" type="hidden" value="<?php echo $area1['idinstitucion'];?>">
        <input id="iduser" name="iduser" type="hidden" value="<?php echo $iduser;?>" >
        <input id="dniuser" name="dniuser" type="hidden" value="<?php echo $dni;?>">
        </li>
        <!-- <a class="nav-link" data-widget="navbar-search" href="#" role="button">
        <i class="fas fa-search"><span style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;font-weight: 500;" > Buscar</span></i>
        </a> -->
    </ul>
    <ul class="navbar-nav ml-auto">
        <!-- Navbar Search -->
        <li class="nav-item">

        <div class="navbar-search-block">
            <form class="form-inline">
            <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Buscar..." aria-label="Search">
                <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                </button>
                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                    <i class="fas fa-times"></i>
                </button>
                </div>
            </div>
            </form>
        </div>
        </li>

        <!-- Messages Dropdown Menu -->

        <!-- Notifications Dropdown Menu -->
        <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
        <i class="fas fa-expand-arrows-alt"></i>
        </a>
    </li>
    <div class="demo-navbar-user nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
            <span class="d-inline-flex flex-lg-row-reverse align-items-center align-middle" style="color:white;">
                <img src="/Sistema_MesaPartes/<?php echo $foto?>" alt class="d-block ui-w-30 rounded-circle">
                <span class="px-1 mr-lg-2 ml-2 ml-lg-0">
                <?php echo utf8_decode($_SESSION['nombre']);?>
                </span>
            </span>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" data-toggle="modal" href="#mimodal">
                <i class="feather icon-power text-danger"></i> &nbsp; Salir</a>
        </div>
    
    </div>
    </ul>
    </nav>
    <!-- /.navbar -->


    <!-- Main Sidebar Container -->
    <aside class="main-sidebar prueba_col2 elevation-4">
      <!-- Brand Logo -->
      <a class="brand-link prueba_col">
        <img src="/Sistema_MesaPartes/<?php echo $row['logo']?>" alt="Logo"
          class="brand-image img-circle elevation-3" style="">
        <span class="brand-text" style="font-weight:600;font-size:1.4rem; color:white;">AFCOOP</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">


        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-black nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" style="color:white;">
            <li class="nav-item ">
              <a href="../../view/Home/" class="nav-link">
                <i class="nav-icon fas fa-home"></i>
                <p>
                  Inicio
                </p>
              </a>
            </li>
            <?php if($area1['area'] == "ADMIN SISTEMA" or $area1['area'] == "ARCHIVO CENTRAL"){?>
            <li class="nav-item">
              <a href="../../view/Usuarios/" class="nav-link">
                <i class="nav-icon fas fa-user"></i>
                <p>
                  Usuarios
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="../../view/Areas/" class="nav-link">
                <i class="nav-icon fas fa-square-full"></i>
                <p>
                  Áreas
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="../../view/Empleados/" class="nav-link">
                <i class="nav-icon fas fa-user-friends"></i>
                <p>
                  Funcionarios
                </p>
              </a>
            </li>
            <li class="nav-item menu-open">
              <a href="../../view/Tramites/" class="nav-link">
                <i class="nav-icon fas fa-folder-open"></i>
                <p>
                Tramites institucionales
                </p>
              </a>
            </li>

            <li class="nav-item menu-open">
              <a href="../../view/Tramites/index2.php" class="nav-link">
                <i class="nav-icon fas fa-folder-open"></i>
                <p>
              Tramites Cooperativos
                </p>
              </a>
            </li>
            <?php }?>
            <li class="nav-item">
              <a href="../../view/NuevoTramite/" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
                <p>
                  Nuevo Documento
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="../../view/TramitesRecibidos/" class="nav-link">
              <i class="nav-icon fas fa-file-import"></i>
                <p>
                  Documentos Recibidos
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="../../view/TramitesEnviados/" class="nav-link">
              <i class="nav-icon fas fa-file-export"></i>
                <p>
                  Documentos Enviados
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="../../view/Busqueda/" class="nav-link">
                <i class="nav-icon fas fa-search-minus"></i>
                <p>
                  Búsqueda de Documentos
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="../../view/informes/" class="nav-link">
                <i class="nav-icon fas fa-file-contract"></i>
                <p>
                  Informes
                </p>
              </a>
            </li>

          </ul>
        </nav>

      </div>
    </aside>

    <!-- INICIO DEL CONTENIDO -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-11">
          <h1 class="titulo_a">SISTEMA DE GESTIÓN DOCUMENTAL</h1>
        </div>
      </div>
    </div>
</div>